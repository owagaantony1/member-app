# Wellness

## Project setup

### Project prerequisites

1. The **Wellness** app runs on Flutter version 1.13.6 **beta channel**. To check the flutter version and channel installed in your machine run `flutter --version`.

2. To switch from another channel of Flutter to the beta channel run `flutter channel beta`.

3. To install Flutter version 1.13.6 run `flutter version v1.13.6`.

Looks like we are done with the prerequisites :-)

### How to setup and run Wellness Application

1. Use `https://gitlab.com/owagaantony1/member-app` to clone this project.

2. Navigate into the project directory.

3. Run `. set_up.sh`

4. Run your emulator. See instructions on how to [run an emulator](https://flutter.dev/docs/get-started/install/linux#set-up-the-android-emulator).

5. Finally run the project running `flutter run` command on your terminal.

## How to setup and run unit, widget and integration tests locally

### Unit Tests

To run unit and widget tests, you will require to install `lcov` by running `sudo apt-get install lcov` in the terminal. Finally, run the following commands in the terminal:

1. From the root folder, run `flutter test --coverage tests/`

2. Run `genhtml -o coverage coverage/lcov.info`

3. To view the coverage, open the html file generated at `coverage/index.html` with your preferred browser.

### Integration tests

To run integration tests, ensure that you have an emulator running. See instructions on how to [run an emulator](https://flutter.dev/docs/get-started/install/linux#set-up-the-android-emulator).

1. Then run `flutter drive --target=test_driver/app.dart`

## Project structure

Here is the folder structure

```
web/
tests/
test_driver/
docs/
assets/
    |- fonts/
    |- images/
    |- audio/
    |- video/
routes/
    |- router_generator.dart
    |- routes.dart
lib/
    |- shared/
        |- constants/
        |- pages/
        |- widgets/
        |- utils/
    |- features/
        |- login/
            |- pages/
            |- widgets/
            |- constants/
            |- store/
    |- store/
        |- reducers/
        |- models/
        |- actions/
        |- middleware/
        |- store.dart
    |- main.dart
pubspec.yaml
set_up.sh
```

## Gitflow workflow

We follow a five-stage gitflow workflow. Below are our git branches in support of the chosen workflow;

1. Develop

2. Internal

3. Alpha

4. Beta

5. Master

