# SILDeferBackNavigation

This component wraps all our pages. In effect it prevents the OS back button from working if it determines that the previous route was an error route.

Usage:

In router generator file, wrap all components with this component.

```dart
return MaterialPageRoute<LoginPage>(builder: (_) => SILDeferBackNavigation(child: LoginPage(), canPop: fromErrorPage));

```