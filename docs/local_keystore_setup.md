# Local Keystore Setup

## Android

### 1. Create keystore file

Certain Google Play services (such as Google Sign-in and App Invites) require you to provide the SHA-1 of your signing certificate. Run the following command in your terminal:

```sh
keytool -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore
```

This creates a new keystore file in the ~/.android folder.

The keytool utility prompts you to enter a password for the keystore. The default password for the debug keystore is android. The keytool then prints the fingerprint to the terminal. For example:

```sh
Certificate fingerprint: SHA1: DA:39:A3:EE:5E:6B:4B:0D:32:55:BF:EF:95:60:18:90:AF:D8:07:09
```

### 2. Add SHA1 fingerprint to Firebase project

Open firebase console in your browser. Navigate to project settings and add the sha1 fingerprint from previous step to the android app settings.

### 3. Create properties file

Open the project in your editor. Create a file called debug_key.properties in the android folder. Place the following within the file:

```txt
storePassword=android
keyPassword=android
keyAlias=androiddebugkey
storeFile=/home/$USER/.android/debug.keystore
```

Save the file and run the project.
