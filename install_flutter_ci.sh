#!/usr/bin/env bash
set -o pipefail
echo "=== INSTALLING FLUTTER ==="
FLUTTER_PATH=$HOME/flutter/bin
git clone --single-branch --branch beta -q https://github.com/flutter/flutter.git $HOME/flutter/
export PATH=PATH="$PATH:/$FLUTTER_PATH"
flutter upgrade && flutter config --no-analytics
flutter --version  
flutter config --enable-web
flutter pub get