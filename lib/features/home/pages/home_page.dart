import 'package:wellness/shared/analytics/amplitude_analytics.dart';
import 'package:wellness/shared/constants/analyticsconstants/analyticsconstants.dart';
import 'package:wellness/shared/widgets/benefit.dart';
import 'package:wellness/shared/widgets/sil_appbar.dart';
import 'package:wellness/shared/widgets/sil_card.dart';
import 'package:wellness/shared/widgets/sil_image_icon_button.dart';
import 'package:wellness/shared/widgets/sill_drawer.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static final Key logoutBtnKey = Key('logout button');

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AmplitudeAnalytics amplitude = AmplitudeAnalytics();

  @override
  void initState() {
    super.initState();
    amplitude.successfulLogin(LoginSuccessful);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: SILAppBar(),
          ),
          Positioned(
            top: 225,
            left: 0,
            right: 0,
            bottom: 0,
            child: ListView(
              padding: EdgeInsets.zero,
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox(height: 20),
                SILNudge(),
                MyCards(),
                MyBenefits(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: MaterialButton(
                    onPressed: () {},
                    child: Text(
                      'View all Benefits',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                      ),
                    ),
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    height: 52,
                    minWidth: 315.28,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                  ),
                ),
                SizedBox(height: 30),
              ],
            ),
          )
        ],
      ),
      drawer: SILDrawer(),
    );
  }
}

class SILNudge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      margin: EdgeInsets.all(15),
      width: double.infinity,
      // height: 130,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.mic, color: Colors.white, size: 35),
              SizedBox(width: 10),
              Expanded(
                  child: Text('Complete your voice recognition setup',
                      style: Theme.of(context)
                          .textTheme
                          .display1
                          .copyWith(color: Colors.white)))
            ],
          ),
          RawMaterialButton(
            constraints: BoxConstraints(minWidth: 140, minHeight: 32),
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4))),
            fillColor: Colors.white,
            onPressed: () {},
            child: Text(
              'Complete setup',
              style: Theme.of(context).textTheme.button,
            ),
          )
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.2, 1],
          colors: [
            Theme.of(context).textSelectionHandleColor,
            Theme.of(context).accentColor
          ],
        ),
      ),
    );
  }
}

class MyCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'My Insurance Cards',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Theme.of(context).textSelectionColor,
                      fontSize: 22),
                ),
                SILImageIconButton(
                  imagePath: 'assets/icons/plus.png',
                  callback: () {},
                  fillColor: Theme.of(context).accentColor,
                )
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            width: double.infinity,
            height: 220,
            child: ListView.separated(
              padding: EdgeInsets.only(right: 20, left: 15),
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              itemCount: 3,
              itemBuilder: (context, position) {
                return SILCard();
              },
              separatorBuilder: (context, position) {
                return SizedBox(width: 15);
              },
            ),
          )
        ],
      ),
    );
  }
}

class MyBenefits extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('My Benefits', style: Theme.of(context).textTheme.display1),
          SizedBox(height: 20),
          Benefit(),
          SizedBox(height: 30),
          Benefit(),
        ],
      ),
    );
  }
}
