class LogoutAction {
  final Function navigationCallback;

  LogoutAction({this.navigationCallback});
}
