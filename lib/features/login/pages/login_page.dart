import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:redux/redux.dart';

import 'package:member-app/features/login/redux/models/login_viewmodel.dart';
import 'package:member-app/features/login/widgets/login_page_content.dart';
import 'package:member-app/redux/models/app_state.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, LoginViewModel>(
      converter: (Store store) {
        return LoginViewModel.fromStore(store);
      },
      builder: (context, loginViewModel) {
        return LoginPageContent(
          loginViewModel: loginViewModel,
        );
      },
    );
  }
}
