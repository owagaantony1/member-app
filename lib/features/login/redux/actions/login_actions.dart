import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

class LoginAction {}

class LoginWithGoogleAction {
  final Function errorCallback;
  final Function successCallback;

  LoginWithGoogleAction({
    @required this.errorCallback,
    @required this.successCallback,
  });
}

class LoginSuccessfulAction {
  final FirebaseUser user;

  LoginSuccessfulAction({@required this.user});
}

class LoginUnsuccessfulAction {
  final error;

  LoginUnsuccessfulAction({this.error});
}
