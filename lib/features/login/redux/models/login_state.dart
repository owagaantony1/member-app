import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class LoginState extends Equatable {
  final bool isLoading;

  LoginState({@required this.isLoading});

  factory LoginState.initial() {
    return LoginState(isLoading: false);
  }

  LoginState copyWith({bool isLoading}) {
    return LoginState(isLoading: isLoading ?? this.isLoading);
  }

  @override
  List<Object> get props => [isLoading];
}
