import 'package:flutter/foundation.dart';

import 'package:redux/redux.dart';

import 'package:member-app/features/login/redux/actions/login_actions.dart';
import 'package:member-app/features/login/redux/models/login_state.dart';
import 'package:member-app/shared/auth/login_exception.dart';
import 'package:member-app/redux/models/app_state.dart';

class LoginViewModel {
  final LoginState loginState;
  final Function(void Function(LoginException) errorCallback,
      VoidCallback successCallback) login;

  LoginViewModel({this.loginState, this.login});

  static LoginViewModel fromStore(Store<AppState> store) {
    return LoginViewModel(
      loginState: store.state.loginState,
      login: (errorCallback, successCallback) {
        store.dispatch(
          LoginWithGoogleAction(
            errorCallback: errorCallback,
            successCallback: successCallback,
          ),
        );
      },
    );
  }
}
