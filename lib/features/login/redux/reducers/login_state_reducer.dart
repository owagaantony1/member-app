import 'package:redux/redux.dart';

import 'package:bewell/features/login/redux/actions/login_actions.dart';
import 'package:bewell/features/login/redux/models/login_state.dart';


final loginStateReducer = combineReducers<LoginState>([
  TypedReducer<LoginState, LoginWithGoogleAction>(_loginWithGoogle),
  TypedReducer<LoginState, LoginSuccessfulAction>(_loginSuccessful),
  TypedReducer<LoginState, LoginUnsuccessfulAction>(_loginError),
]);

LoginState _loginWithGoogle(
    LoginState loginState, LoginWithGoogleAction action) {
  return loginState.copyWith(isLoading: true);
}

LoginState _loginSuccessful(
    LoginState loginState, LoginSuccessfulAction action) {
  return loginState.copyWith(isLoading: false);
}

LoginState _loginError(LoginState loginState, LoginUnsuccessfulAction action) {
  return loginState.copyWith(isLoading: false);
}
