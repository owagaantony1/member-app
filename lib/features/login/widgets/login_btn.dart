import 'package:flutter/material.dart';

import 'package:member-app/features/login/redux/models/login_viewmodel.dart';
import 'package:member-app/router/routes.dart';
import 'package:member-app/shared/analytics/analytics_provider.dart';
import 'package:member-app/shared/auth/login_exception.dart';
import 'package:member-app/shared/constants/analyticsconstants/analyticsconstants.dart';


class LoginWithGoogleButton extends StatelessWidget {
  LoginWithGoogleButton({
    Key key,
    @required this.loginViewModel,
    @required this.scaffoldKey,
  }) : super(key: key);

  final LoginViewModel loginViewModel;
  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 80,
      left: 52,
      right: 52,
      child: Container(
        width: double.infinity,
        height: 54,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(48),
          color: Colors.white,
        ),
        child: MaterialButton(
          onPressed: () {
            AnalyticsProvider.of(context)
                .amplitudeAnalytics
                ?.collectLogs(LoginButtonTapped);

            loginViewModel.login(
              (LoginException error) {
                final snackBar = _getSnackBarWithText(error.cause);
                scaffoldKey.currentState.showSnackBar(snackBar);
              },
              () {
                Navigator.of(context).pushReplacementNamed(Routes.terms);
                AnalyticsProvider.of(context)
                    .amplitudeAnalytics
                    ?.successfulLogin(LoginSuccessful);
              },
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image(
                width: 35,
                height: 35,
                image: AssetImage('assets/images/google_logo.png'),
              ),
              SizedBox(width: 15),
              Text(
                'Sign in with Google',
                style: Theme.of(context).textTheme.subhead.copyWith(
                    color: Theme.of(context).textSelectionHandleColor),
              ),
            ],
          ),
        ),
      ),
    );
  }

  SnackBar _getSnackBarWithText(String text) {
    return SnackBar(
      content: Text(text),
    );
  }
}
