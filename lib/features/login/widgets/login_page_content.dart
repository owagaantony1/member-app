import 'package:flutter/material.dart';

import 'package:member-app/features/login/redux/models/login_viewmodel.dart';
import 'package:member-app/features/login/widgets/logo_box.dart';
import 'package:member-app/shared/utils/utils.dart';

import 'login_btn.dart';

Globals _globals = Globals();

class LoginPageContent extends StatelessWidget {
  final LoginViewModel loginViewModel;

  LoginPageContent({this.loginViewModel});

  @override
  Widget build(BuildContext context) {
    bool isLoading = loginViewModel.loginState.isLoading;
    return Scaffold(
      key: _globals.scaffoldKey,
      body: Center(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: SILUtils.backgroundGradient(),
          ),
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    LogoBox(isLoading: isLoading),
                    SizedBox(height: 15),
                    Text(
                      'Be.Well',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 15),
                    Text(
                      'Effortless Access to Care',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 50),
                  ],
                ),
              ),
              LoginWithGoogleButton(
                key: Key('LoginWithGoogleButton'),
                loginViewModel: loginViewModel,
                scaffoldKey: _globals.scaffoldKey,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Globals {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
}
