import 'package:flutter/material.dart';

class LogoBox extends StatelessWidget {
  const LogoBox({
    Key key,
    @required this.isLoading,
  }) : super(key: key);

  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: isLoading
          ? Padding(
              padding: EdgeInsets.all(16),
              child: CircularProgressIndicator(),
            )
          : null,
      decoration: BoxDecoration(
          color: Color(0xff4D000000),
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          image: isLoading
              ? null
              : DecorationImage(image: AssetImage('assets/images/logo.png'))),
    );
  }
}
