import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

import 'package:redux/redux.dart';

import 'package:member-app/features/profile_setup/redux/models/dob_viewmodel.dart';
import 'package:member-app/features/profile_setup/widgets/dob_page_content.dart';
import 'package:member-app/redux/models/app_state.dart';

class DobPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DobViewmodel>(
      converter: (Store<AppState> store) {
        return DobViewmodel.fromStore(store);
      },
      builder: (context, DobViewmodel viewModel) {
        return DobPageContent(dobViewModel: viewModel);
      },
    );
  }
}
