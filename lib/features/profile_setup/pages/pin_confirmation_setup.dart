import 'package:member-app/router/routes.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:member-app/shared/utils/utils.dart';

class PinConfirmationSetup extends StatefulWidget {
  const PinConfirmationSetup({Key key, @required this.arguments})
      : super(key: key);

  final dynamic arguments;

  @override
  _PinConfirmationSetupState createState() => _PinConfirmationSetupState();
}

class _PinConfirmationSetupState extends State<PinConfirmationSetup> {
  var confirmFourDigitPin;

  @override
  Widget build(BuildContext context) {
    var fourDigitPin = widget.arguments;
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: SILUtils.backgroundGradient(),
          ),
          child: ListView(
            padding: EdgeInsets.only(left: 28, right: 28),
            children: <Widget>[
              SizedBox(
                height: 140,
              ),
              Text(
                'Re-enter pin to confirm',
                style: Theme.of(context).textTheme.headline.copyWith(
                  color: Theme.of(context).backgroundColor,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 60,
              ),
              SizedBox(
                height: 20,
              ),
              PinCodeTextField(
                key: Key('confirmation_pin_code_text_field'),
                wrapAlignment: WrapAlignment.center,
                maxLength: 4,
                pinBoxHeight: 62.69,
                pinBoxWidth: 50,
                pinBoxRadius: 10,
                defaultBorderColor: Colors.white,
                hasTextBorderColor: Colors.white,
                maskCharacter: '\u25CF',
                pinTextStyle: TextStyle(color: Theme.of(context).backgroundColor),
                hideCharacter: true,
                autofocus: true,
                onDone: (value) {
                  confirmFourDigitPin = value;
                  if (confirmFourDigitPin == fourDigitPin) {
                    Navigator.of(context).pushNamed(Routes.home);
                  } else {
                    Navigator.of(context).pushNamed(Routes.pinSetup);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
