import 'package:member-app/router/routes.dart';
import 'package:member-app/shared/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

class PinSetup extends StatefulWidget {
  @override
  _PinSetupState createState() => _PinSetupState();
}

class _PinSetupState extends State<PinSetup> {
  var fourDigitPin;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: SILUtils.backgroundGradient(),
        ),
        child: ListView(
          padding: EdgeInsets.only(left: 28, right: 28),
          children: <Widget>[
            SizedBox(
              height: 80,
            ),
            Text(
              'Set a  four digit pin to secure your account',
              style: Theme.of(context).textTheme.headline.copyWith(
                    color: Theme.of(context).backgroundColor,
                  ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 60,
            ),
            Text(
              'Enter your PIN',
              textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle.copyWith(
                  color: Theme.of(context).backgroundColor,
                ),
            ),
            SizedBox(
              height: 60,
            ),
            PinCodeTextField(
              key: Key('pin_code_text_field'),
              wrapAlignment: WrapAlignment.center,
              maxLength: 4,
              pinBoxHeight: 62.69,
              pinBoxWidth: 50,
              pinBoxRadius: 10,
              defaultBorderColor: Colors.white,
              hasTextBorderColor: Colors.white,
              maskCharacter: '\u25CF',
              pinTextStyle: TextStyle(color: Theme.of(context).backgroundColor),
              hideCharacter: true,
              autofocus: true,
              onDone: (value) {
                fourDigitPin = value;
                print(fourDigitPin);
                Navigator.of(context).pushNamed(Routes.pinConfirmationSetup,
                    arguments: fourDigitPin);
              },
            ),
          ],
        ),
      ),
    );
  }
}
