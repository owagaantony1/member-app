import 'package:member-app/features/profile_setup/constants/profile_setup_contants.dart';
import 'package:member-app/router/routes.dart';
import 'package:flutter/material.dart';

class TermsAndConditionsPage extends StatefulWidget {
  @override
  _TermsAndConditionsPageState createState() => _TermsAndConditionsPageState();
}

class _TermsAndConditionsPageState extends State<TermsAndConditionsPage> {
  bool isNotChecked = false;

  @override
  Widget build(BuildContext context) {
    var _onPressed;
    if (isNotChecked) {
      _onPressed = () {
        Navigator.of(context).pushNamed(Routes.dobPage);
      };
    }
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 22,
              ),
              Icon(
                Icons.assignment,
                size: 48,
                color: Color(0xFF6319DD),
              ),
              SizedBox(
                height: 14,
              ),
              Text(
                'Terms and Conditions',
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(
                height: 22,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Scrollbar(
                    controller: ScrollController(keepScrollOffset: true),
                    child: ListView(
                      children: <Widget>[
                        ListTile(
                          subtitle: Text('$termsAndConditions',
                          style: Theme.of(context).textTheme.body1,
                              textAlign: TextAlign.justify),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Padding(
                padding: EdgeInsets.only(left: 24),
                child: Row(children: <Widget>[
                  Checkbox(
                    key: Key('check_box'),
                    value: isNotChecked,
                    onChanged: (bool value) {
                      setState(() {
                        _onPressed = value;
                        isNotChecked = value;
                      });
                    },
                  ),
                  Text(
                    'I accept',
                    style: Theme.of(context).textTheme.body1,
                  ),
                  Text(
                    ' Terms and Conditions',
                    style: Theme.of(context).textTheme.body2,
                  ),
                ]),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 36),
                width: double.infinity,
                child: MaterialButton(
                  key: Key('continue_button'),
                  onPressed: _onPressed,
                  child: Text(
                    'Continue',
                    style: Theme.of(context).textTheme.button.copyWith(
                      color: Theme.of(context).backgroundColor,
                    ),
                  ),
                  color: Color(0xFF7FBBCA),
                  textColor: Color(0xFFFFFFFF),
                  height: 52,
                  minWidth: 315.28,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0)),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 14),
                child: MaterialButton(
                  key: Key('cancel_button'),
                  child: Text(
                    'Cancel',
                    style: Theme.of(context).textTheme.button,
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacementNamed(Routes.login);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
