import 'package:redux/redux.dart';

import 'package:member-app/features/profile_setup/redux/actions/dob_page_actions.dart';
import 'package:member-app/features/profile_setup/redux/models/profile_state.dart';

final profileStateReducer = combineReducers<ProfileState>([
  TypedReducer<ProfileState, SaveDobPageDetailsAction>(_saveDobPageDetails),
]);

ProfileState _saveDobPageDetails(
    ProfileState state, SaveDobPageDetailsAction action) {
  return state.copyWith(dob: action.dob, gender: action.gender);
}
