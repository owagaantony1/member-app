import 'package:flutter/foundation.dart';

import 'package:bewell/features/profile_setup/gender.dart';

class SaveDobPageDetailsAction {
  DateTime dob;
  Gender gender;

  SaveDobPageDetailsAction({
    @required this.dob,
    @required this.gender,
  });
}
