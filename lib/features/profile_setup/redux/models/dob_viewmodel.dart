import 'package:redux/redux.dart';

import 'package:member-app/features/profile_setup/redux/actions/dob_page_actions.dart';
import 'package:member-app/features/profile_setup/redux/models/profile_state.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/features/profile_setup/gender.dart';

class DobViewmodel {
  final ProfileState profileState;
  final Function(DateTime dob, Gender gender) saveUserDetails;

  DobViewmodel({this.profileState, this.saveUserDetails});

  static DobViewmodel fromStore(Store<AppState> store) {
    return DobViewmodel(
        profileState: store.state.profileState,
        saveUserDetails: (DateTime dob, Gender gender) {
          store.dispatch(SaveDobPageDetailsAction(dob: dob, gender: gender));
        });
  }
}
