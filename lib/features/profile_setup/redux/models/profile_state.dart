import 'package:equatable/equatable.dart';

import 'package:bewell/features/profile_setup/gender.dart';

class ProfileState extends Equatable {
  final bool acceptTermsAndConditions;
  final Gender gender;
  final DateTime dob;

  ProfileState({this.acceptTermsAndConditions, this.gender, this.dob});

  factory ProfileState.initial() {
    return ProfileState(
      acceptTermsAndConditions: false,
      gender: Gender.Female,
      dob: null,
    );
  }

  ProfileState copyWith({DateTime dob, Gender gender}) {
    return ProfileState(dob: dob ?? this.dob, gender: gender ?? this.gender);
  }

  @override
  List<Object> get props => [acceptTermsAndConditions, gender, dob];
}
