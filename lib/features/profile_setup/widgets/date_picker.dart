import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  final Function(DateTime) onChanged;

  DatePicker({
    Key key,
    @required this.onChanged,
  }) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  final TextEditingController _dobController = TextEditingController();

  @override
  void dispose() {
    _dobController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          _selectedDate(context);
        },
        child: AbsorbPointer(
          child: TextFormField(
            validator: (String date) {
              if (date.isEmpty || date == null) {
                return 'Please select date';
              }
              return null;
            },
            controller: _dobController,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(6),
              ),
              labelText: 'Date of birth',
              labelStyle: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
              hintStyle: TextStyle(backgroundColor: Colors.white),
              suffixIcon: Icon(
                Icons.calendar_today,
                color: Colors.white,
              ),
            ),
          ),
        ));
  }

  void _selectedDate(BuildContext context) async {
    var picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1969),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return child;
      },
    );

    if (picked != null) {
      _dobController.text = _convertDateToString(picked);
      widget.onChanged(picked);
    }
  }

  String _convertDateToString(DateTime datePicked) {
    return DateFormat('dd-MM-yyyy').format(datePicked);
  }
}
