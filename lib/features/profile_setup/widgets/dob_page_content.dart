import 'package:flutter/material.dart';

import 'package:member-app/features/profile_setup/gender.dart';
import 'package:member-app/features/profile_setup/redux/models/dob_viewmodel.dart';
import 'package:member-app/features/profile_setup/widgets/date_picker.dart';
import 'package:member-app/features/profile_setup/widgets/gender_picker.dart';
import 'package:member-app/router/routes.dart';
import 'package:member-app/shared/utils/utils.dart';

class DobPageContent extends StatefulWidget {
  static final nextButtonKey = Key('next_button');
  static final datePickerKey = Key('date-picker');
  static final genderPickerKey = Key('gender-picker');

  final DobViewmodel dobViewModel;

  DobPageContent({@required this.dobViewModel});

  @override
  _DobPageContentState createState() => _DobPageContentState();
}

class _DobPageContentState extends State<DobPageContent> {
  final _formKey = GlobalKey<FormState>();
  Gender _genderPicked;
  DateTime _datePicked;

  bool _formIsValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(32.0),
        decoration: BoxDecoration(
          gradient: SILUtils.backgroundGradient(),
        ),
        child: Center(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Text(
                    'Just a little more ...',
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 54,
                  ),
                  DatePicker(
                    key: DobPageContent.datePickerKey,
                    onChanged: (DateTime dateTime) {
                      setState(() {
                        _datePicked = dateTime;
                        _formIsValid = _formKey.currentState.validate();
                      });
                    },
                  ),
                  SizedBox(
                    height: 42,
                  ),
                  GenderPicker(
                    key: DobPageContent.genderPickerKey,
                    initialValue: _genderPicked,
                    onChanged: (Gender gender) {
                      setState(() {
                        _genderPicked = gender;
                        _formIsValid = _formKey.currentState.validate();
                      });
                    },
                  ),
                  SizedBox(
                    height: 106,
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    child: RaisedButton(
                      key: DobPageContent.nextButtonKey,
                      child: Text(
                        'Next',
                        style: TextStyle(fontSize: 16),
                      ),
                      onPressed: _formIsValid ? _saveForm : null,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _saveForm() {
    widget.dobViewModel.saveUserDetails(_datePicked, _genderPicked);
    Navigator.of(context).pushNamedAndRemoveUntil(
        Routes.pinSetup, ModalRoute.withName(Routes.login));
  }
}
