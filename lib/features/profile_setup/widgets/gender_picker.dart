import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:bewell/features/profile_setup/gender.dart';

class GenderPicker extends StatelessWidget {
  final Gender initialValue;
  final Function(Gender) onChanged;

  const GenderPicker({
    Key key,
    @required this.initialValue,
    @required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<Gender>(
      validator: (Gender gender) {
        return gender == null ? 'please choose an option' : null;
      },
      value: initialValue,
      itemHeight: 48,
      iconEnabledColor: Colors.white,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
        hintText: 'Gender',
        hintStyle: TextStyle(color: Colors.white, fontSize: 14),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.transparent),
        ),
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
        ),
      ),
      icon: Icon(Icons.arrow_drop_down),
      items: Gender.values
          .map((gender) => DropdownMenuItem(
                value: gender,
                child: Text(
                  describeEnum(gender),
                  key: ValueKey(gender),
                  style: TextStyle(fontSize: 14, color: Colors.black),
                ),
              ))
          .toList(),
      selectedItemBuilder: (context) {
        return Gender.values
            .map((gender) => Text(
                  describeEnum(gender),
                  style: TextStyle(color: Colors.white),
                ))
            .toList();
      },
      onChanged: (Gender gender) {
        onChanged(gender);
      },
    );
  }
}
