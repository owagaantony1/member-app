import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:redux/redux.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:member-app/shared/profile/profile_details_provider.dart';
import 'package:member-app/router/router_generator.dart';
import 'package:member-app/router/routes.dart';
import 'package:member-app/shared/analytics/amplitude_analytics.dart';
import 'package:member-app/shared/analytics/analytics_provider.dart';
import 'package:member-app/shared/auth/auth_provider.dart';
import 'package:member-app/shared/constants/apptheme/apptheme.dart';
import 'package:member-app/shared/gql/gql.dart';
import 'package:member-app/redux/actions/auth_status_action.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/models/auth_status.dart';
import 'package:member-app/redux/store.dart';
import 'package:member-app/shared/constants/be_well.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  GoogleSignIn _googleSignIn = GoogleSignIn();
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  AuthProvider authProvider = AuthProvider(
    googleSignIn: _googleSignIn,
    firebaseAuth: _firebaseAuth,
  );

  member-app.sharedPrefs = await SharedPreferences.getInstance();
  ProfileProvider profileProvider = ProfileProvider(
    sharedPreferences: member-app.sharedPrefs,
  );

  final store = createStore(authProvider, profileProvider);
  final AmplitudeAnalytics amplitudeAnalytics = AmplitudeAnalytics();
  NavigatorObserver navigatorObserver = NavigatorObserver();

  runApp(
    member-appApp(
      store: store,
      navigatorObserver: navigatorObserver,
      amplitudeAnalytics: kIsWeb ? null : amplitudeAnalytics,
    ),
  );
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class member-appApp extends StatefulWidget {
  final Store<AppState> store;
  final AmplitudeAnalytics amplitudeAnalytics;
  final NavigatorObserver navigatorObserver;

  const member-appApp({
    Key key,
    @required this.navigatorObserver,
    @required this.store,
    @required this.amplitudeAnalytics,
  }) : super(key: key);

  @override
  _member-appAppState createState() => _member-appAppState();

  @override
  String toStringShort() {
    return 'be.well.app.main';
  }
}

class _member-appAppState extends State<member-appApp> {
  @override
  void initState() {
    super.initState();
    widget.store.dispatch(CheckAuthState(successCallback: () {
      navigatorKey.currentState.pushReplacementNamed(Routes.home);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      key: Key('global-store-provider'),
      store: widget.store,
      child: StoreConnector<AppState, AuthStatus>(
        converter: (Store store) {
          return store.state.authStatus;
        },
        builder: (BuildContext context, AuthStatus authStatus) {
          return AnalyticsProvider(
            amplitudeAnalytics: widget.amplitudeAnalytics,
            child: GraphQLProvider(
                client: CreateGQLClient(),
                child: CacheProvider(
                  child: MaterialApp(
                    theme: member-appTheme,
                    debugShowCheckedModeBanner: false,
                    initialRoute: authStatus == AuthStatus.signedIn
                        ? Routes.home
                        : Routes.login,
                    onGenerateRoute: RouteGenerator.generateRoute,
                    navigatorKey: navigatorKey,
                    navigatorObservers: [widget.navigatorObserver],
                  ),
                )),
          );
        },
      ),
    );
  }
}
