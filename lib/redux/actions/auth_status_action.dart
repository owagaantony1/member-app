import 'package:member-app/redux/models/auth_status.dart';

class AuthStatusAction {
  final AuthStatus authStatus;

  AuthStatusAction({this.authStatus});
}

class CheckAuthState {
  final Function successCallback;

  CheckAuthState({this.successCallback});
}

class CheckFirebaseUserAuthState {}

class AuthStatusError {
  final error;

  AuthStatusError({this.error});
}
