import 'package:flutter/foundation.dart';

import 'package:redux/redux.dart';

import 'package:member-app/features/home/redux/actions/home_actions.dart';
import 'package:member-app/features/login/redux/actions/login_actions.dart';
import 'package:member-app/shared/auth/auth_provider.dart';
import 'package:member-app/shared/auth/login_exception.dart';
import 'package:member-app/redux/actions/auth_status_action.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/models/auth_status.dart';



class AuthMiddleware extends MiddlewareClass<AppState> {
  final AuthProvider authProvider;

  AuthMiddleware({@required this.authProvider});

  @override
  void call(Store<AppState> store, action, next) {
    next(action);

    if (action is LoginWithGoogleAction) {
      _signInWithGoogle(store, action, next);
    }

    if (action is CheckAuthState) {
      _checkAuthState(store, action, next);
    }

    if (action is LogoutAction) {
      _logout(store, action, next);
    }
  }

  void _signInWithGoogle(Store<AppState> store, LoginWithGoogleAction action,
      NextDispatcher next) async {
    try {
      final user = await authProvider.signInWithGoogle();
      next(LoginSuccessfulAction(user: user));
      next(AuthStatusAction(authStatus: AuthStatus.signedIn));
      if (action.successCallback != null) {
        action.successCallback();
      }
    } on LoginException catch (e) {
      next(LoginUnsuccessfulAction(error: e));
      next(AuthStatusError(error: e));
      if (action.errorCallback != null) {
        action.errorCallback(e);
      }
    } catch (e) {
      next(LoginUnsuccessfulAction(error: e));
      next(AuthStatusError(error: e));
    }
  }

  void _checkAuthState(
      Store<AppState> store, CheckAuthState action, NextDispatcher next) async {
    try {
      final user = await authProvider.currentUser();
      if (user != null) {
        next(LoginSuccessfulAction(user: user));
        next(AuthStatusAction(authStatus: AuthStatus.signedIn));
        if (action.successCallback != null) {
          action.successCallback();
        }
      } else {
        next(AuthStatusAction(authStatus: AuthStatus.notSignedIn));
      }
    } catch (e) {
      next(AuthStatusError(error: e));
    }
  }

  void _logout(
      Store<AppState> store, LogoutAction action, NextDispatcher next) async {
    await authProvider.logout();
    if (action.navigationCallback != null) {
      action.navigationCallback();
    }
    next(AuthStatusAction(authStatus: AuthStatus.notSignedIn));
  }
}
