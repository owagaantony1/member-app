import 'package:flutter/foundation.dart';

import 'package:redux/redux.dart';

import 'package:member-app/features/profile_setup/redux/actions/dob_page_actions.dart';
import 'package:member-app/redux/actions/profile_action.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/shared/profile/profile_details_provider.dart';

class ProfileMiddleWare extends MiddlewareClass<AppState> {
  final ProfileProvider profileProvider;

  ProfileMiddleWare({@required this.profileProvider});

  @override
  void call(Store<AppState> store, action, next) {
    next(action);

    if (action is SaveDobPageDetailsAction) {
      _saveDobPageDetails(store, action, next);
    }
  }

  void _saveDobPageDetails(Store<AppState> store,
      SaveDobPageDetailsAction action, NextDispatcher next) async {
    try {
      profileProvider.saveDobPageDetails(
        gender: action.gender,
        dob: action.dob,
      );
    } catch (e) {
      print('$e');
      next(ProfileSaveErrorAction(error: e));
    }
  }
}
