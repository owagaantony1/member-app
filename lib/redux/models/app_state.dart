import 'package:firebase_auth/firebase_auth.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'package:member-app/features/profile_setup/redux/models/profile_state.dart';
import 'package:member-app/features/login/redux/models/login_state.dart';
import 'package:member-app/redux/models/auth_status.dart';

@immutable
class AppState extends Equatable {
  final FirebaseUser user;
  final AuthStatus authStatus;
  final LoginState loginState;
  final ProfileState profileState;

  AppState({
    @required this.user,
    @required this.authStatus,
    @required this.loginState,
    @required this.profileState,
  });

  factory AppState.initial() {
    return AppState(
      user: null,
      authStatus: AuthStatus.notSignedIn,
      loginState: LoginState.initial(),
      profileState: ProfileState.initial(),
    );
  }

  @override
  List<Object> get props => [user, authStatus, loginState, profileState];
}
