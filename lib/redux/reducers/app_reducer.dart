import 'package:member-app/features/login/redux/reducers/login_state_reducer.dart';
import 'package:member-app/features/profile_setup/reducers/profile_state_reducer.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/reducers/auth_status_reducer.dart';
import 'package:member-app/redux/reducers/user_reducer.dart';

AppState appReducer(AppState state, dynamic action) {
  return AppState(
    user: userReducer(state.user, action),
    authStatus: authStatusReducer(state.authStatus, action),
    loginState: loginStateReducer(state.loginState, action),
    profileState: profileStateReducer(state.profileState, action),
  );
}
