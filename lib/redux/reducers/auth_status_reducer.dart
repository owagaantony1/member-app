import 'package:redux/redux.dart';

import 'package:member-app/redux/actions/auth_status_action.dart';
import 'package:member-app/redux/models/auth_status.dart';


final authStatusReducer = combineReducers<AuthStatus>([
  TypedReducer<AuthStatus, AuthStatusAction>(_handleAuthStatus),
  TypedReducer<AuthStatus, AuthStatusError>(_handleError),
]);

AuthStatus _handleAuthStatus(AuthStatus status, AuthStatusAction action) {
  return action.authStatus;
}

AuthStatus _handleError(AuthStatus status, AuthStatusError error) {
  return AuthStatus.errorSigningIn;
}
