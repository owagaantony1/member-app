import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import 'package:bewell/features/home/redux/actions/home_actions.dart';
import 'package:bewell/features/login/redux/actions/login_actions.dart';


final userReducer = combineReducers<FirebaseUser>([
  TypedReducer<FirebaseUser, LoginSuccessfulAction>(_login),
  TypedReducer<FirebaseUser, LoginUnsuccessfulAction>(_loginError),
  TypedReducer<FirebaseUser, LogoutAction>(_logout),
]);

FirebaseUser _login(FirebaseUser user, LoginSuccessfulAction action) {
  return action.user;
}

FirebaseUser _loginError(FirebaseUser user, LoginUnsuccessfulAction action) {
  return null;
}

FirebaseUser _logout(FirebaseUser user, LogoutAction action) {
  return null;
}
