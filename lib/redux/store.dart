import 'package:redux/redux.dart';

import 'package:member-app/redux/middleware/profile_middeware.dart';
import 'package:member-app/shared/profile/profile_details_provider.dart';
import 'package:member-app/shared/auth/auth_provider.dart';
import 'package:member-app/redux/middleware/auth_middleware.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/reducers/app_reducer.dart';

Store<AppState> createStore(AuthProvider authProvider,
    [ProfileProvider profileProvider]) {
  return Store<AppState>(
    appReducer,
    initialState: AppState.initial(),
    middleware: [
      AuthMiddleware(authProvider: authProvider),
      ProfileMiddleWare(profileProvider: profileProvider),
    ],
  );
}
