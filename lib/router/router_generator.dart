import 'package:member-app/features/home/pages/home_page.dart';
import 'package:member-app/features/login/pages/login_page.dart';
import 'package:member-app/features/profile_setup/pages/dob_page.dart';
import 'package:member-app/features/profile_setup/pages/pin_confirmation_setup.dart';
import 'package:member-app/features/profile_setup/pages/pin_setup.dart';
import 'package:member-app/features/profile_setup/pages/terms_and_conditions.dart';
import 'package:member-app/shared/pages/404.dart';
import 'package:member-app/shared/widgets/SIL_defer_back_navigation.dart';
import 'package:flutter/material.dart';

import 'routes.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    bool fromErrorPage = args == 'fromErrorPage' ? false : true;

    switch (settings.name) {
      case Routes.login:
        return MaterialPageRoute<LoginPage>(
            builder: (_) => SILDeferBackNavigation(
                child: LoginPage(), canPop: fromErrorPage));
      case Routes.home:
        return MaterialPageRoute<HomePage>(builder: (_) => HomePage());
      case Routes.dobPage:
        return MaterialPageRoute<DobPage>(builder: (_) => DobPage());
      case Routes.terms:
        return MaterialPageRoute<TermsAndConditionsPage>(
            builder: (_) => SILDeferBackNavigation(
                child: TermsAndConditionsPage(), canPop: fromErrorPage));
      case Routes.pinSetup:
        return MaterialPageRoute<PinSetup>(builder: (_) => PinSetup());
        break;
      case Routes.pinConfirmationSetup:
        return MaterialPageRoute<PinConfirmationSetup>(
            builder: (_) => PinConfirmationSetup(arguments: args));
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute<Route_404>(builder: (_) {
      return Route_404(whereToUrl: Routes.login);
    });
  }
}
