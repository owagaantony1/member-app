class Routes {
  static const String home = '/home';
  static const String login = '/';
  static const String terms = '/termsandconditions';
  static const String dobPage = '/dobPage';
  static const String pinSetup = '/pinSetup';
  static const String pinConfirmationSetup = '/pinConfirmationSetup';
}
