import 'package:amplitude_flutter/amplitude_flutter.dart';

class AmplitudeAnalytics {
  final AmplitudeFlutter analytics =
      AmplitudeFlutter('04530b0215b2a5a256bcbc2202e81e24');

  Future<void> collectLogs(String eventName) async {
    await analytics.logEvent(name: eventName);
  }

  Future<void> successfulLogin(String actionName) async {
    await analytics.logEvent(name: actionName);
  }
}
