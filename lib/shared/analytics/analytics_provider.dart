import 'package:bewell/shared/analytics/amplitude_analytics.dart';
import 'package:flutter/material.dart';

class AnalyticsProvider extends InheritedWidget {
  final AmplitudeAnalytics amplitudeAnalytics;

  AnalyticsProvider({@required this.amplitudeAnalytics, Widget child})
      : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }

  static AnalyticsProvider of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AnalyticsProvider>();
  }
}
