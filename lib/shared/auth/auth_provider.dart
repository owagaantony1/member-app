import 'package:bewell/shared/auth/login_exception.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthProvider {
  final FirebaseAuth firebaseAuth;
  final GoogleSignIn googleSignIn;

  AuthProvider({@required this.firebaseAuth, @required this.googleSignIn});

  Future<FirebaseUser> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      return (await firebaseAuth.signInWithCredential(credential)).user;
    } else {
      throw LoginException('No account selected');
    }
  }

  Future<FirebaseUser> currentUser() {
    return firebaseAuth.currentUser();
  }

  Future<void> logout() {
    return firebaseAuth.signOut();
  }
}
