class LoginException implements Exception {
  String cause;

  LoginException(this.cause);

  @override
  String toString() {
    return 'LoginException ${this.cause}';
  }
}
