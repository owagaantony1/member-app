import 'package:flutter/material.dart';
import 'package:bewell/shared/constants/apptheme/colors.dart';

final ThemeData bewellTheme = buildTheme();
//use accordingly with guidance from the title
ThemeData buildTheme() {
  final ThemeData base = ThemeData.light();

  return base.copyWith(
    //cyan
    accentColor: bewellPrimaryColor,

    //grey
    backgroundColor: bewellBackgroundColor,

    //red
    errorColor: bewellRed,

    //text
    textSelectionColor: bewellTextColor,

    textSelectionHandleColor: bewellTextColorPurple,

    textTheme: _buildBewellAppTextTheme(base.textTheme),
    primaryTextTheme: _buildBewellAppTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildBewellAppTextTheme(base.accentTextTheme),
  );
}

TextTheme _buildBewellAppTextTheme(TextTheme base) {
  return base
      .copyWith(
        //use this for non-bold headlines accordingly as in the designs
        headline:
            base.headline.copyWith(fontSize: 24.0, fontWeight: FontWeight.w400),
        //mainly used on the AppBar titles
        display1:
            base.display1.copyWith(fontSize: 20.0, fontWeight: FontWeight.w400),
        //card titles
        title: base.title.copyWith(fontSize: 20.0, fontWeight: FontWeight.w700),
        //all subtitles
        subtitle:
            base.subtitle.copyWith(fontSize: 16.0, fontWeight: FontWeight.w400),
        //section titles
        subhead:
            base.subhead.copyWith(fontSize: 18.0, fontWeight: FontWeight.w700),
        //paragraph text
        body1: base.body1.copyWith(fontSize: 14.0, fontWeight: FontWeight.w400),
        //applicable where there are caption fonts sized
        caption:
            base.caption.copyWith(fontSize: 12.0, fontWeight: FontWeight.w400),
      )
      .apply(fontFamily: 'RedHatDisplay', displayColor: bewellTextColor);
}
