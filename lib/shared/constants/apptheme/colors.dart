import 'package:flutter/material.dart';

//cyan
final bewellPrimaryColor = Color(0xFF5ACAD8);
//light grey
final bewellBackgroundColor = Color(0xFFF4F6FA);
//red
final bewellRed = Color(0xFFE70000);
//darkblue
final bewellTextColor = Color(0xFF0E2153);

//purple
final bewellTextColorPurple = Color(0xFF6A54B7);
