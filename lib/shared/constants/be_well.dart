import 'package:shared_preferences/shared_preferences.dart';

import 'package:bewell/shared/profile/profile_details_provider.dart';

class BeWell {
  static SharedPreferences sharedPrefs;
  static ProfileProvider profileDetailsProvider;
}
