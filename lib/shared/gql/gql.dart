import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

 ValueNotifier<GraphQLClient> CreateGQLClient(){
  final HttpLink httpLink = HttpLink(uri: 'https://api.github.com/graphql');
  final AuthLink authLink = AuthLink(
    getToken: () async => 'Bearer bcae5b3d94a67b91065cf771127659ae55702fc5',    
  );
  final Link link = authLink.concat(httpLink);
  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      cache: NormalizedInMemoryCache(
      dataIdFromObject: typenameDataIdFromObject,
    ),
      link: link,
    ),
  );
  return client;
}


 