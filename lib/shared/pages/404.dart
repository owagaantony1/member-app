import 'package:flutter/material.dart';

class Route_404 extends StatelessWidget {

  Route_404({@required this.whereToUrl, this.whereToText = 'GO BACK'});

  final String whereToUrl;
  final String whereToText;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: 200,
                  height: 200,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                      border: Border.all(width: 1, color: Colors.greenAccent),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 0,
                            color: Colors.grey.withOpacity(0.03),
                            spreadRadius: 10),
                      ]),
                  child: Center(
                    child:
                        Icon(Icons.priority_high, size: 100, color: Colors.red),
                  ),
                ),
                SizedBox(height: 50),
                Text(
                  'An error occured',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(height: 10),
                Text('We can\'t seem to find what you are looking for'),
              ],
            ),
            RawMaterialButton(
              onPressed: () {
                Navigator.pushNamed(context, whereToUrl, arguments: 'fromErrorPage');
              },
              child: Text(whereToText),
            )
          ],
        ),
      ),
    );
  }
}
