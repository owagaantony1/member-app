import 'package:flutter/foundation.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:member-app/features/profile_setup/gender.dart';
import 'package:member-app/shared/constants/be_well.dart';
import 'package:member-app/shared/utils/utils.dart';

class ProfileProvider {
  final String _genderSharedPredKey = 'gender';
  final String _dobSharedPredKey = 'dob';

  final SharedPreferences sharedPreferences;

  ProfileProvider({@required this.sharedPreferences});

  void saveDobPageDetails({Gender gender, DateTime dob}) async {
    await Future.wait([
      member-app.sharedPrefs.setString(_genderSharedPredKey, describeEnum(gender)),
      member-app.sharedPrefs.setString(_dobSharedPredKey,
          SILUtils.convertDateToString(date: dob, format: 'dd-MM-yyyy')),
    ]);
  }
}
