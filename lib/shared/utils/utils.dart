import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

class SILUtils {
  static LinearGradient backgroundGradient() => LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        stops: [0, 0.7],
        colors: [
          Color(0xff6A54B7),
          Color(0xff7FBBCA),
        ],
      );

  static String convertDateToString({
    @required DateTime date,
    @required String format,
  }) {
    return DateFormat(format).format(date);
  }
}
