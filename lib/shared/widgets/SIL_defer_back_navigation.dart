import 'package:flutter/material.dart';

class SILDeferBackNavigation extends StatelessWidget {
  SILDeferBackNavigation({@required this.child, this.canPop = true});

  // used to determine whether to veto attempts by the user to go back to previous page
  final bool canPop;

  final Widget child;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: child,
      onWillPop: () async => canPop,
    );
  }
}
