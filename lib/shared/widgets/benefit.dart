import 'package:flutter/material.dart';

class Benefit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(4),
        ),
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.04),
              blurRadius: 24,
              offset: Offset(0, 12))
        ],
      ),
      constraints: BoxConstraints(
        maxWidth: 430,
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
            child: Row(
              children: <Widget>[
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      border: Border.all(color: Color(0xffEEEEEE))),
                  child: Center(
                    child: Text('OP', style: TextStyle(fontSize: 26)),
                  ),
                ),
                SizedBox(width: 20),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Outpatient Enhanced II Shared',
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        // style: TextStyle(
                        //   color: Color(0xff0E2153),
                        //   fontSize: 20,
                        //   fontWeight: FontWeight.w500,
                        // ),
                        style: Theme.of(context).textTheme.headline,
                      ),
                      Text(
                        'Caring Insurance',
                        style: TextStyle(
                          color: Color(0xff2196F3),
                          // fontSize: 20,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      Text(
                        'Available for use',
                        style: TextStyle(
                          color: Color(0xff797979),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: Color(0xffDADADA).withOpacity(0.3),
                ),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('42% used',
                        style: Theme.of(context).textTheme.subtitle),
                    SizedBox(height: 2),
                    Container(
                      width: 230,
                      height: 5,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: double.infinity,
                            color: Color(0xffDADADA),
                          ),
                          Positioned(
                            top: 0,
                            left: 0,
                            right: 230 - ((42 / 100) * 230),
                            bottom: 0,
                            child: Container(
                              width: double.infinity,
                              height: double.infinity,
                              color: Color(0xff5ACAD8),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                RawMaterialButton(
                  fillColor: Color(0xffEFF3F3),
                  elevation: 0.5,
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  onPressed: () {},
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2))),
                  child: Row(
                    children: <Widget>[
                      Text('VIEW'),
                      Icon(Icons.keyboard_arrow_right)
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
