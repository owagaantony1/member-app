import 'package:camera/camera.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';

enum DeviceScreenType { Mobile, Tablet, Desktop }

// our responsive builder widget
// class ResponsiveBuilder extends StatelessWidget {
//   // builder function
//   final Widget Function(
//       BuildContext context, SizingInformation sizingInformation) builder;

//   ResponsiveBuilder({Key key, this.builder}) : super(key: key);

//   final screenType = ScreenType();
//   @override
//   Widget build(BuildContext context) {
//     return LayoutBuilder(builder: (context, boxConstraints) {
//       var mediaQuery = MediaQuery.of(context);
//       var sizingInformation = SizingInformation(
//         deviceScreenType: screenType.getDeviceType(),
//         screenSize: mediaQuery.size,
//         localWidgetSize:
//             Size(boxConstraints.maxWidth, boxConstraints.maxHeight),
//       );

//       return builder(context, sizingInformation);
//     });
//   }
// }

// gets the device orientation
class OrientationLayout extends StatelessWidget {
  final Widget landscape;
  final Widget portrait;

  const OrientationLayout({Key key, this.landscape, this.portrait})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.landscape) {
      return landscape ?? portrait;
    }
    return portrait;
  }
}

// util to get the device type based on the sizing info from MediaQuery object
class ScreenType {
  final MediaQueryData mediaQueryData;
  ScreenType({this.mediaQueryData});
  DeviceScreenType getDeviceType() {
    double deviceWidth = this.mediaQueryData.size.shortestSide;
    if (deviceWidth > 950) {
      return DeviceScreenType.Desktop;
    } else if (deviceWidth > 600) {
      return DeviceScreenType.Tablet;
    } else {
      return DeviceScreenType.Mobile;
    }
  }
}

// checks device capabilities
class DeviceCapabilities {
  static bool hasMicrophone;
  CameraController cameraController;
  final LocalAuthentication authentication;
  final CameraCapabilities cameraCapabilities;
  final Connectivity deviceConnectivity;

  DeviceCapabilities(
      {this.cameraCapabilities, this.authentication, this.deviceConnectivity});

  Future checkBiometrics() async {
    List<BiometricType> availableBiometrics =
        await authentication.getAvailableBiometrics();
    return availableBiometrics;
  }

// check if cameras are available in the device. if no cameras
// are available then the microphone is not available and vice versa
  Future<bool> checkCameras() async {
    bool hasCamera;
    await cameraCapabilities.availableCameras().then((availableCams) {
      if (availableCams.isEmpty) {
        hasCamera = false;
      } else {
        hasCamera = true;
      }
    });
    return hasCamera;
  }

// checks if there is an internet connectivity
  Future<String> checkConnectivity() async {
    var connectivity = await deviceConnectivity.checkConnectivity();
    String connectivityType;
    switch (connectivity) {
      case ConnectivityResult.mobile:
        connectivityType = 'MOBILE';
        break;
      case ConnectivityResult.wifi:
        connectivityType = 'WIFI';
        break;
      default:
        connectivityType = 'UNAVAILABLE';
        break;
    }
    return connectivityType;
  }
}

// Compiles information about the size of the screen that is fed to the
// builder function so that it can build the appropriate UI
class SizingInformation {
  final DeviceScreenType deviceScreenType;
  final Size screenSize;
  final Size localWidgetSize;

  SizingInformation(
      {this.deviceScreenType, this.screenSize, this.localWidgetSize});
}

class CameraCapabilities {
  Future<List<CameraDescription>> availableCameras() {
    return availableCameras();
  }
}
