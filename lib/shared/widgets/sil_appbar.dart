import 'package:member-app/shared/widgets/sil_image_icon_button.dart';
import 'package:flutter/material.dart';

class SILAppBar extends StatelessWidget {
  SILAppBar({this.useLargeAppBar = true});

  final bool useLargeAppBar;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(14, 30, 15, 0),
      width: double.infinity,
      height: useLargeAppBar ? 230 : 90,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: useLargeAppBar
                ? MainAxisAlignment.spaceBetween
                : MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SILImageIconButton(
                imagePath: useLargeAppBar
                    ? 'assets/icons/menu.png'
                    : 'assets/icons/bell.png',
                callback: useLargeAppBar
                    ? () {
                        Scaffold.of(context).openDrawer();
                      }
                    : () {
                        Navigator.of(context).pop();
                      },
              ),
              if (useLargeAppBar)
                SILImageIconButton(
                  imagePath: 'assets/icons/bell.png',
                  callback: () {},
                  hasBadge: true,
                  badgeValue: '2',
                ),
              if (!useLargeAppBar) ...[
                SizedBox(width: 15),
                Text(
                  'Outpatient Shared benefit',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                )
              ]
            ],
          ),
          if(useLargeAppBar) // also add a check whether there are any new notifications
          Container(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
            width: double.infinity,
            height: 135,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 10),
                Text(
                  'Hello Stacey,',
                  style: TextStyle(
                    fontSize: 26,
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 15),
                GestureDetector(
                  onTap: () {},
                  child: Text.rich(
                    TextSpan(
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                      children: [
                        TextSpan(text: 'You have '),
                        TextSpan(
                          text: '  2 appointments  ',
                          style: TextStyle(
                            backgroundColor: Colors.white,
                            fontWeight: FontWeight.w500,
                            height: 2,
                            color: Colors.black,
                          ),
                        ),
                        TextSpan(text: ' on Tuesday 19 Nov, 2019 at 11:00 am'),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: useLargeAppBar ? BorderRadius.only(
          bottomLeft: Radius.elliptical(500, 30),
          bottomRight: Radius.elliptical(500, 30),
        ) : null,
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.2, 1],
          colors: [
            Color(0xff6A54B7),
            Color(0xff7FBBCA),
          ],
        ),
      ),
    );
  }
}