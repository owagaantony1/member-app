import 'package:flutter/material.dart';

class SILCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 380,
      height: 220,
      decoration: BoxDecoration(
        color: Color(0xff5ACAD8),
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 10,
            right: -10,
            child: Opacity(
              opacity: 0.1,
              child: cardDecoration(),
            ),
          ),
          Positioned(
            bottom: -150,
            left: 50,
            child: Opacity(
              opacity: 0.1,
              child: cardDecoration(),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            width: 380,
            bottom: 0,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerRight,
                    child: Image(
                      image: AssetImage('assets/images/slade_logo.png'),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child:
                        cardText(label: 'Member Number', value: '56678-345535'),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      cardText(
                          label: 'Name of insured', value: 'Stacey Possible'),
                      cardText(
                        label: 'Date issued',
                        value: '01 July 2019',
                        width: 100
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

Widget cardText({String label, String value, double width = 160}) => Container(
      width: width,
      child: Text.rich(
        TextSpan(
          style: TextStyle(
            color: Colors.white,
          ),
          children: [
            TextSpan(
              text: '${label} \n',
            ),
            TextSpan(
              text: value,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
            )
          ],
        ),
        overflow: TextOverflow.ellipsis,
      ),
    );

Widget cardDecoration() => Container(
      width: 220,
      height: 220,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color(0xff6A54B7),
                Color(0xff7FBBCA),
              ])),
    );
