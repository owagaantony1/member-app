import 'package:flutter/material.dart';

class SILImageIconButton extends StatelessWidget {
  SILImageIconButton({
    @required this.imagePath,
    @required this.callback,
    this.fillColor,
    this.hasBadge = false,
    this.badgeColor = const Color(0xffE70000),
    this.badgeValue,
    this.elevation = 0,
  });

  final String imagePath;
  final Function callback;
  final Color fillColor;
  final double elevation;

  final bool hasBadge;
  final String badgeValue;
  final Color badgeColor;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: fillColor,
      onPressed: callback,
      elevation: elevation,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
      constraints: BoxConstraints(
        minHeight: 44,
        minWidth: 44,
      ),
      child: Center(
        child: Container(
          width: 34,
          height: 34,
          child: hasBadge
              ? Stack(
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Positioned(
                      top: -5,
                      right: -5,
                      child: Container(
                        width: 24,
                        height: 24,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: badgeColor,
                        ),
                        child: Center(
                          child: Text(
                            badgeValue,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              : null,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.contain,
              image: AssetImage(imagePath),
            ),
          ),
        ),
      ),
    );
  }
}
