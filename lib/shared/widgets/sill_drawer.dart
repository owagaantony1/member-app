import 'package:member-app/features/home/redux/actions/home_actions.dart';
import 'package:member-app/router/routes.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class SILDrawer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  SILDrawerHeader(),
                  DrawerLink(linkText: 'My Benefits', callback: () {}),
                  DrawerLink(linkText: 'My Appointments', callback: () {}),
                  DrawerLink(linkText: 'My Orders', callback: () {}),
                  DrawerLink(linkText: 'Health Coach', callback: () {}),
                  DrawerLink(linkText: 'My Health', callback: () {}),
                  DrawerLink(linkText: 'Link Insurance card', callback: () {}),
                  DrawerLink(linkText: 'Verification reques', callback: () {}),
                  DrawerLink(linkText: 'My Appointments', callback: () {}),
                  DrawerLink(linkText: 'My Orders', callback: () {}),
                  DrawerLink(linkText: 'Health Coach', callback: () {}),
                  DrawerLink(linkText: 'My Health', callback: () {}),
                  DrawerLink(linkText: 'Link Insurance card', callback: () {}),
                  SizedBox(height: 60)
                ],
              ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              width: double.infinity,
              height: 50,
              color: Colors.grey[100],
              child: StoreConnector<AppState, VoidCallback>(
                converter: (Store store) {
                  return () {
                    store.dispatch(LogoutAction(navigationCallback: () {
                      Navigator.of(context).pushReplacementNamed(Routes.login);
                    }));
                  };
                },
                builder: (BuildContext context, callback) {
                  return RawMaterialButton(
                    key: Key('logout_button'),
                    onPressed: callback,
                    child: Text(
                      'Logout',
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}

class SILDrawerHeader extends StatelessWidget {
  const SILDrawerHeader();
  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      padding: EdgeInsets.all(15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 70,
            height: 70,
            child: Center(
              child: Text(
                'SP',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                ),
              ),
            ),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xff7FBBCA),
            ),
          ),
          SizedBox(width: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Stacey Possible',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              ),
              GestureDetector(
                key: Key('view_profile'),
                onTap: () {},
                child: Text(
                  'View Profile',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class DrawerLink extends StatelessWidget {
  DrawerLink({
    @required this.linkText,
    @required this.callback,
  });

  final String linkText;
  final Function callback;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.only(left: 25),
      title: Text(
        linkText,
        style: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 16,
        ),
      ),
      onTap: callback,
    );
  }
}
