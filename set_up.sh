#!/usr/bin/env bash

# run flutter pub get

flutter pub get

# set up git hooks

ln -s ../../.git_hooks/pre-push.sh .git/hooks/pre-push
chmod +x .git_hooks/pre-push.sh