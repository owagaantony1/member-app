import 'package:mockito/mockito.dart';

import 'package:member-app/shared/profile/profile_details_provider.dart';
import 'package:member-app/shared/auth/auth_provider.dart';

class MockAuthProvider extends Mock implements AuthProvider {}

class MockProfileProvider extends Mock implements ProfileProvider {}
