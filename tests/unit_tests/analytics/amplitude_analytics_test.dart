import 'package:bewell/shared/analytics/amplitude_analytics.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'mocks.dart';

void main() {
  test('should collect logd', () async {
    final mock = MockAmplitudeAnalytics();
    final amplitudeAnalytics = AmplitudeAnalytics();
    String LoginButtonTapped = 'login_button_tapped';
    Future<void> go() async {}
    ;
    when(mock.collectLogs(LoginButtonTapped)).thenAnswer((_) => go());
    await amplitudeAnalytics.collectLogs(LoginButtonTapped);
  });

  test('should log a successful login', () async {
    final mock = MockAmplitudeAnalytics();
    final amplitudeAnalytics = AmplitudeAnalytics();
    String LoginSuccessful = 'login_successful';
    Future<void> go() async {}
    ;
    when(mock.successfulLogin(LoginSuccessful)).thenAnswer((_) => go());
    await amplitudeAnalytics.successfulLogin(LoginSuccessful);
  });
}
