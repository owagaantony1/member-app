import 'package:bewell/shared/analytics/amplitude_analytics.dart';
import 'package:mockito/mockito.dart';

class MockAmplitudeAnalytics extends Mock implements AmplitudeAnalytics {}