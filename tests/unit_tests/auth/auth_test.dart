import 'package:member-app/shared/auth/auth_provider.dart';
import 'package:member-app/shared/auth/login_exception.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'firebaseauth_mock.dart';

void main() {
  group('AuthProvider', () {
    final FirebaseUserMock firebaseUser = FirebaseUserMock();
    FirebaseAuthMock firebaseAuth;
    GoogleSignInMock googleSignIn;

    GoogleSignInAccountMock googleSignInAccount;

    GoogleSignInAuthenticationMock googleSignInAuthentication;

    AuthResultMock authResult;

    AuthProvider authProvider;

    setUp(() {
      googleSignInAccount = GoogleSignInAccountMock();
      googleSignInAuthentication = GoogleSignInAuthenticationMock();
      authResult = AuthResultMock();
      firebaseAuth = FirebaseAuthMock();
      googleSignIn = GoogleSignInMock();

      authProvider = AuthProvider(
        googleSignIn: googleSignIn,
        firebaseAuth: firebaseAuth,
      );
    });

    test('signInWithGoogle() returns a FirebaseUser', () async {
      when(googleSignIn.signIn()).thenAnswer(
          (_) => Future<GoogleSignInAccountMock>.value(googleSignInAccount));

      when(googleSignInAccount.authentication).thenAnswer((_) =>
          Future<GoogleSignInAuthenticationMock>.value(
              googleSignInAuthentication));

      when(firebaseAuth.signInWithCredential(any))
          .thenAnswer((_) => Future<AuthResultMock>.value(authResult));

      final user = await authProvider.signInWithGoogle();
      expect(user, firebaseUser);
      verify(googleSignIn.signIn()).called(1);
      verify(googleSignInAccount.authentication).called(1);
      verify(firebaseAuth.signInWithCredential(any)).called(1);
    });

    test('currentUser() returns currentUser', () async {
      when(authProvider.currentUser())
          .thenAnswer((_) => Future<FirebaseUser>.value(firebaseUser));

      expect(await authProvider.currentUser(), firebaseUser);
      verify(firebaseAuth.currentUser()).called(1);
    });

    test(
        'throws LoginException when no account is selected by for signInWithGoogle',
        () async {
      when(googleSignIn.signIn()).thenAnswer((_) => Future<Null>.value(null));

      expect(authProvider.signInWithGoogle(), throwsA(isA<LoginException>()));

      expect(
        authProvider.signInWithGoogle(),
        throwsA(
          predicate(
            (e) => e is LoginException && e.cause == 'No account selected',
          ),
        ),
      );
    });

    test('logout() logs out the user', () async {
      when(authProvider.logout()).thenAnswer((_) => Future<void>.value(null));

      await authProvider.logout();
      verify(firebaseAuth.signOut()).called(1);
    });
  });
}
