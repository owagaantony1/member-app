import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mockito/mockito.dart';

class FirebaseAuthMock extends Mock implements FirebaseAuth {}

class GoogleSignInMock extends Mock implements GoogleSignIn {}

class GoogleSignInAccountMock extends Mock implements GoogleSignInAccount {}

class GoogleSignInAuthenticationMock extends Mock
    implements GoogleSignInAuthentication {
  @override
  String get idToken => 'id';

  @override
  String get accessToken => 'access_token';
}

class AuthCredentialMock extends Mock implements AuthCredential {}

class AuthResultMock extends Mock implements AuthResult {
  FirebaseUserMock user = FirebaseUserMock();
}

class FirebaseUserMock extends Mock implements FirebaseUser {
  @override
  String get displayName => 'John Doe';

  @override
  String get uid => 'uid';

  @override
  String get email => 'johndoe@mail.com';

  @override
  String get photoUrl => 'http://www.adityag.me';

  bool operator ==(o) => o is FirebaseUser && o.uid == uid;
}
