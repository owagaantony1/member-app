import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:mockito/mockito.dart';

import 'package:member-app/main.dart';
import 'package:member-app/redux/store.dart';
import '../mocks.dart';
import '../widgets_tests/mocks.dart';
import 'analytics/mocks.dart';
import 'auth/firebaseauth_mock.dart';

void main() {
  group('main', () {
    MockAuthProvider authProvider;
    FirebaseUserMock firebaseUser;
    MockAmplitudeAnalytics amplitudeAnalytics;
    MockNavigatorObserver navigatorObserver;

    setUp(() {
      authProvider = MockAuthProvider();
      firebaseUser = FirebaseUserMock();
      amplitudeAnalytics = MockAmplitudeAnalytics();
      navigatorObserver = MockNavigatorObserver();
    });

    test('Test main returns BewllApp', () {
      when(authProvider.currentUser())
          .thenAnswer((_) => Future<FirebaseUser>.value(firebaseUser));

      final store = createStore(authProvider);
      final Widget rootWidget = member-appApp(
        navigatorObserver: navigatorObserver,
        amplitudeAnalytics: amplitudeAnalytics,
        store: store,
      );
      expect(rootWidget.toStringShort(), 'be.well.app.main');
    });
  });
}
