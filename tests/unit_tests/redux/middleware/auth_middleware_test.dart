import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:mockito/mockito.dart';

import 'package:member-app/features/home/redux/actions/home_actions.dart';
import 'package:member-app/features/login/redux/actions/login_actions.dart';
import 'package:member-app/redux/actions/auth_status_action.dart';
import 'package:member-app/redux/middleware/auth_middleware.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/models/auth_status.dart';

import '../../../mocks.dart';
import '../../auth/firebaseauth_mock.dart';
import '../mocks.dart';

final FirebaseUserMock firebaseUser = FirebaseUserMock();

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  group('AuthMiddleware', () {
    final actionLog = <dynamic>[];
    final next = (dynamic action) => actionLog.add(action);

    MockStore mockStore;
    MockAuthProvider mockAuthProvider;
    AuthMiddleware middleware;

    setUp(() {
      mockAuthProvider = MockAuthProvider();
      middleware = AuthMiddleware(authProvider: mockAuthProvider);
      mockStore = MockStore();

      when(mockStore.state).thenReturn(AppState.initial());
    });

    tearDown(() {
      actionLog.clear();
    });

    test('dispatches LoginSuccessfulAction on successful login', () async {
      when(mockAuthProvider.signInWithGoogle())
          .thenAnswer((_) => Future<FirebaseUser>.value(firebaseUser));

      when(mockAuthProvider.currentUser())
          .thenAnswer((_) => Future<FirebaseUser>.value(firebaseUser));

      await middleware.call(
          mockStore,
          LoginWithGoogleAction(
            successCallback: null,
            errorCallback: null,
          ),
          next);

      expect(actionLog.length, 3);
      expect(actionLog[0], isA<LoginWithGoogleAction>());
      expect(actionLog[1], isA<LoginSuccessfulAction>());
      expect(actionLog[1].user, firebaseUser);
      expect(actionLog[2], isA<AuthStatusAction>());
    });

    test('dispatches LoginUnsuccessfulAction on error', () async {
      final error = Exception();
      when(mockAuthProvider.signInWithGoogle()).thenThrow(error);

      await middleware.call(
          mockStore,
          LoginWithGoogleAction(
            successCallback: null,
            errorCallback: null,
          ),
          next);

      expect(actionLog.length, 3);
      expect(actionLog[1], isA<LoginUnsuccessfulAction>());
      expect(actionLog[1].error, error);
      expect(actionLog[2], isA<AuthStatusError>());
      expect(actionLog[2].error, error);
    });

    test(
        'when called with CheckAuthState dispatches LoginSuccessfulAction if user is logged in',
        () async {
      when(mockAuthProvider.currentUser())
          .thenAnswer((_) => Future<FirebaseUser>.value(firebaseUser));

      await middleware.call(mockStore, CheckAuthState(), next);

      expect(actionLog.length, 3);
      expect(actionLog[0], isA<CheckAuthState>());
      expect(actionLog[1], isA<LoginSuccessfulAction>());
      expect(actionLog[1].user, firebaseUser);
      expect(actionLog[2], isA<AuthStatusAction>());
      expect(actionLog[2].authStatus, AuthStatus.signedIn);
    });

    test(
        'when called with CheckAuthState dispatches AuthStatusAction if user is not logged in',
        () async {
      when(mockAuthProvider.currentUser())
          .thenAnswer((_) => Future<FirebaseUser>.value(null));

      await middleware.call(mockStore, CheckAuthState(), next);

      expect(actionLog.length, 2);
      expect(actionLog[0], isA<CheckAuthState>());
      expect(actionLog[1], isA<AuthStatusAction>());
      expect(actionLog[1].authStatus, AuthStatus.notSignedIn);
    });

    test(
        'when called with CheckAuthState dispatches AuthStatusError if error occured',
        () async {
      final error = Exception();
      when(mockAuthProvider.currentUser()).thenThrow(error);

      await middleware.call(mockStore, CheckAuthState(), next);

      expect(actionLog.length, 2);
      expect(actionLog[1], isA<AuthStatusError>());
      expect(actionLog[1].error, error);
    });

    test('changes auth status when LogoutAction called', () async {
      when(mockAuthProvider.logout())
          .thenAnswer((_) => Future<void>.value(Void));

      await middleware.call(mockStore, LogoutAction(), next);

      expect(actionLog.length, 2);
      expect(actionLog[1], isA<AuthStatusAction>());
      expect(actionLog[1].authStatus, AuthStatus.notSignedIn);
    });
  });
}
