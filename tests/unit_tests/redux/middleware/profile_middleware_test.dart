import 'package:member-app/features/profile_setup/gender.dart';
import 'package:member-app/features/profile_setup/redux/actions/dob_page_actions.dart';
import 'package:member-app/redux/actions/profile_action.dart';
import 'package:member-app/redux/middleware/profile_middeware.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../mocks.dart';
import '../mocks.dart';

void main() {
  group('Profile middleware', () {
    final actionLog = <dynamic>[];
    final next = (dynamic action) => actionLog.add(action);

    MockProfileProvider profileProvider;
    ProfileMiddleWare profileMiddleWare;
    MockStore mockStore;

    setUp(() {
      profileProvider = MockProfileProvider();
      profileMiddleWare = ProfileMiddleWare(profileProvider: profileProvider);
      mockStore = MockStore();

      when(mockStore.state).thenReturn(AppState.initial());
    });

    tearDown(() {
      actionLog.clear();
    });

    test(
        'should save DobPageDetails details when SaveDobPageDetailsAction is dispatched',
        () async {
      when(profileProvider.saveDobPageDetails(
              gender: anyNamed('gender'), dob: anyNamed('dob')))
          .thenReturn(null);

      await profileMiddleWare.call(
          mockStore,
          SaveDobPageDetailsAction(
            gender: Gender.Male,
            dob: DateTime.now(),
          ),
          next);

      expect(actionLog.length, 1);
      expect(actionLog[0], isA<SaveDobPageDetailsAction>());
    });

    test(
        'when error occurs during saving DobPageDetails should dispatch ProfileSaveErrorAction',
        () async {
      final error = Exception();
      when(profileProvider.saveDobPageDetails(
              gender: anyNamed('gender'), dob: anyNamed('dob')))
          .thenThrow(error);

      await profileMiddleWare.call(
          mockStore,
          SaveDobPageDetailsAction(
            gender: Gender.Male,
            dob: DateTime.now(),
          ),
          next);

      expect(actionLog.length, 2);
      expect(actionLog[1], isA<ProfileSaveErrorAction>());
      expect(actionLog[1].error, error);
    });
  });
}
