import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';

import 'package:bewell/redux/models/app_state.dart';

class MockStore extends Mock implements Store<AppState> {}
