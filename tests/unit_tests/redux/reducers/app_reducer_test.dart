import 'package:flutter_test/flutter_test.dart';

import 'package:member-app/features/profile_setup/redux/models/profile_state.dart';
import 'package:member-app/features/login/redux/models/login_state.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/models/auth_status.dart';
import 'package:member-app/redux/reducers/app_reducer.dart';

void main() {
  group('appReducer', () {
    test('should return initial state', () {
      final appState = AppState(
        profileState: ProfileState.initial(),
        authStatus: AuthStatus.notSignedIn,
        user: null,
        loginState: LoginState.initial(),
      );
      var reducer = appReducer(appState, null);

      expect(reducer, AppState.initial());
    });
  });
}
