import 'package:flutter_test/flutter_test.dart';

import 'package:member-app/redux/actions/auth_status_action.dart';
import 'package:member-app/redux/models/auth_status.dart';
import 'package:member-app/redux/reducers/auth_status_reducer.dart';

void main() {
  group('authStatusReducer', () {
    test('should return initial state', () {
      var reducer = authStatusReducer(AuthStatus.notSignedIn, null);

      expect(reducer, AuthStatus.notSignedIn);
    });

    test('should respond to AuthStatusAction', () {
      var reducer = authStatusReducer(
        AuthStatus.notSignedIn,
        AuthStatusAction(authStatus: AuthStatus.signedIn),
      );

      expect(reducer, AuthStatus.signedIn);
    });

    test('should respond to AuthStatusError', () {
      var reducer = authStatusReducer(
        AuthStatus.notSignedIn,
        AuthStatusError(error: Exception()),
      );

      expect(reducer, AuthStatus.errorSigningIn);
    });
  });
}
