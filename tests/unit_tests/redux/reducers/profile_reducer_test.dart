import 'package:member-app/features/profile_setup/redux/actions/dob_page_actions.dart';
import 'package:test/test.dart';

import 'package:member-app/features/profile_setup/gender.dart';
import 'package:member-app/features/profile_setup/reducers/profile_state_reducer.dart';
import 'package:member-app/features/profile_setup/redux/models/profile_state.dart';

void main() {
  group('profileStateReducer', () {
    test('should return initial state', () {
      final profileState = ProfileState(
        acceptTermsAndConditions: false,
        gender: Gender.Female,
        dob: null,
      );

      var reducer = profileStateReducer(profileState, null);

      expect(reducer, ProfileState.initial());
    });

    test('should handle SaveDobPageDetailsAction', () {
      var reducer = profileStateReducer(
        ProfileState.initial(),
        SaveDobPageDetailsAction(
          gender: Gender.Female,
          dob: DateTime.now(),
        ),
      );

      expect(reducer, isA<ProfileState>());
    });
  });
}
