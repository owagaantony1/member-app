import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:member-app/features/home/redux/actions/home_actions.dart';
import 'package:member-app/features/login/redux/actions/login_actions.dart';
import 'package:member-app/redux/reducers/user_reducer.dart';
import '../../auth/firebaseauth_mock.dart';

final FirebaseUserMock firebaseUser = FirebaseUserMock();

void main() {
  group('userReducer', () {
    test('should return initial state', () {
      var reducer = userReducer(null, null);
      expect(reducer, null);
    });

    test('should handle LoginSuccessfulAction', () {
      var reducer = userReducer(
        null,
        LoginSuccessfulAction(user: firebaseUser),
      );

      expect(reducer, isA<FirebaseUser>());
    });

    test('should handle LoginUnsuccessfullAction', () {
      var reducer = userReducer(
        null,
        LoginUnsuccessfulAction(),
      );

      expect(reducer, null);
    });

    test('should handle LogoutAction', () {
      var reducer = userReducer(firebaseUser, LogoutAction());

      expect(reducer, null);
    });
  });
}
