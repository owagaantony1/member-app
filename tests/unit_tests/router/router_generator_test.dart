import 'package:member-app/router/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:member-app/router/router_generator.dart';

void main() {
  final routeGenerator = RouteGenerator.generateRoute;
  test('Test router returns LoginPage', () {
    RouteSettings settings =
        RouteSettings(name: Routes.login, isInitialRoute: false);
    expect(routeGenerator(settings).runtimeType.toString(),
        'MaterialPageRoute<LoginPage>');
  });

  test('Test router returns HomePage', () {
    RouteSettings settings =
        RouteSettings(name: Routes.home, isInitialRoute: false);
    expect(routeGenerator(settings).runtimeType.toString(),
        'MaterialPageRoute<HomePage>');
  });

  test('Test router returns TermsAndConditionsPage', () {
    RouteSettings settings =
        RouteSettings(name: Routes.terms, isInitialRoute: false);
    expect(routeGenerator(settings).runtimeType.toString(),
        'MaterialPageRoute<TermsAndConditionsPage>');
  });

  // TODO
  // test a route with arguments

  test('Test router returns errorPage', () {
    RouteSettings settings =
        RouteSettings(name: '/loginx', isInitialRoute: false);
    expect(routeGenerator(settings).runtimeType.toString(),
        'MaterialPageRoute<Route_404>');
  });
}
