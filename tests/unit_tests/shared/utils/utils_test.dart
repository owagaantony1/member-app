import 'package:test/test.dart';

import 'package:bewell/shared/utils/utils.dart';

void main() {
  group('SILUtils', () {
    test('convertDateToString should return correctly formatted date', () {
      var date = DateTime.utc(2020, DateTime.january, 31);
      var expected = '31-01-2020';
      var formattedDate =
          SILUtils.convertDateToString(date: date, format: 'dd-MM-yyyy');

      expect(formattedDate, expected);
    });
  });
}
