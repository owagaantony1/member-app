import 'package:bewell/shared/widgets/responsive_builder.dart';
import 'package:camera/camera.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:local_auth/local_auth.dart';
import 'package:mockito/mockito.dart';

class MockSizingInformation extends Mock implements SizingInformation {}

class MockDeviceCapabilities extends Mock implements DeviceCapabilities {}

class MockLocalAuthentication extends Mock implements LocalAuthentication {}

class MockCameraCapabilities extends Mock implements CameraCapabilities {}

class MockConnectivity extends Mock implements Connectivity {}

class MockScreenType extends Mock implements ScreenType {}

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  group('ScreenType', () {
    test('screen size', () {
      var screenType =
          ScreenType(mediaQueryData: MediaQueryData(size: Size(1920, 1080)));
      var deviceScreenType = screenType.getDeviceType();
      expect(deviceScreenType, DeviceScreenType.Desktop);
      screenType =
          ScreenType(mediaQueryData: MediaQueryData(size: Size(1280, 720)));
      deviceScreenType = screenType.getDeviceType();
      expect(deviceScreenType, DeviceScreenType.Tablet);
      screenType =
          ScreenType(mediaQueryData: MediaQueryData(size: Size(480, 360)));
      deviceScreenType = screenType.getDeviceType();
      expect(deviceScreenType, DeviceScreenType.Mobile);
    });
    // test('CameraCapabilities', () {
    //   final camCapabilities = CameraCapabilities();
    //   when(camCapabilities.availableCameras())
    //       .thenAnswer((_) => Future<List<CameraDescription>>.value([
    //             CameraDescription(
    //                 name: 'Camera One',
    //                 lensDirection: CameraLensDirection.front,
    //                 sensorOrientation: 90)
    //           ]));
    //   camCapabilities.availableCameras();
    // });
  });

  group('DeviceCapabilites', () {
    LocalAuthentication localAuthentication;
    CameraCapabilities cameraCapabilities;
    Connectivity connectivity;

    setUp(() {
      localAuthentication = MockLocalAuthentication();
      cameraCapabilities = MockCameraCapabilities();
      connectivity = MockConnectivity();
    });

    test('it should test the sizing info class', () {
      Size screenSize = Size(600, 900);
      DeviceScreenType screenType = DeviceScreenType.Desktop;
      Size widgetSize = Size(100, 200);
      var sizingInfo = SizingInformation(
          deviceScreenType: screenType,
          screenSize: screenSize,
          localWidgetSize: widgetSize);
      expect(sizingInfo.screenSize, Size(600, 900));
    });

    test('should test biometrics', () async {
      WidgetsFlutterBinding.ensureInitialized();

      DeviceCapabilities deviceCapabilities = DeviceCapabilities(
          authentication: localAuthentication,
          cameraCapabilities: cameraCapabilities);

      List<BiometricType> availableBiometrics = [BiometricType.fingerprint];
      when(localAuthentication.getAvailableBiometrics()).thenAnswer(
          (_) => Future<List<BiometricType>>.value(availableBiometrics));

      var biometrics = await deviceCapabilities.checkBiometrics();
      expect(biometrics, availableBiometrics);
    });

    test('should check cameras', () async {
      DeviceCapabilities deviceCapabilities = DeviceCapabilities(
          authentication: localAuthentication,
          cameraCapabilities: cameraCapabilities,
          deviceConnectivity: connectivity);

      List<CameraDescription> cameras = [
        CameraDescription(
            name: 'Camera One',
            lensDirection: CameraLensDirection.front,
            sensorOrientation: 90)
      ];
      when(cameraCapabilities.availableCameras())
          .thenAnswer((_) => Future<List<CameraDescription>>.value(cameras));
      var capabilities = await deviceCapabilities.checkCameras();
      expect(capabilities, true);
      when(cameraCapabilities.availableCameras())
          .thenAnswer((_) => Future<List<CameraDescription>>.value([]));
      capabilities = await deviceCapabilities.checkCameras();
      expect(capabilities, false);
    });

    test('should check connectivity', () async {
      DeviceCapabilities deviceCapabilities = DeviceCapabilities(
          authentication: localAuthentication,
          cameraCapabilities: cameraCapabilities,
          deviceConnectivity: connectivity);
      when(connectivity.checkConnectivity()).thenAnswer(
          (_) => Future<ConnectivityResult>.value(ConnectivityResult.mobile));
      var conn = await deviceCapabilities.checkConnectivity();
      expect(conn, 'MOBILE');
      when(connectivity.checkConnectivity()).thenAnswer(
          (_) => Future<ConnectivityResult>.value(ConnectivityResult.wifi));
      conn = await deviceCapabilities.checkConnectivity();
      expect(conn, 'WIFI');
    });
  });
}
