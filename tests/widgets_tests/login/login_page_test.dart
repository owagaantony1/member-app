import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';

import 'package:member-app/features/login/pages/login_page.dart';
import 'package:member-app/features/login/redux/models/login_viewmodel.dart';
import 'package:member-app/features/login/widgets/login_page_content.dart';
import 'package:member-app/features/profile_setup/pages/terms_and_conditions.dart';
import 'package:member-app/router/router_generator.dart';
import 'package:member-app/shared/analytics/analytics_provider.dart';
import 'package:member-app/shared/auth/login_exception.dart';
import 'package:member-app/shared/constants/analyticsconstants/analyticsconstants.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/store.dart';

import '../../mocks.dart';
import '../../unit_tests/analytics/mocks.dart';
import '../../unit_tests/auth/firebaseauth_mock.dart';
import '../mocks.dart';

void main() {
  group('LoginPage', () {
    MockNavigatorObserver observer;
    LoginViewModel loginViewModel;
    Store<AppState> store;
    MockAuthProvider mockAuthProvider;
    MockAmplitudeAnalytics amplitudeAnalytics;

    final FirebaseUserMock firebaseUser = FirebaseUserMock();

    setUp(() {
      observer = MockNavigatorObserver();
      mockAuthProvider = MockAuthProvider();

      store = createStore(mockAuthProvider);
      loginViewModel = LoginViewModel.fromStore(store);
      amplitudeAnalytics = MockAmplitudeAnalytics();

      Future<void> go() async {}
      when(amplitudeAnalytics.collectLogs(LoginButtonTapped))
          .thenAnswer((_) => go());
    });

    Future<void> _buildLoginPageContent(WidgetTester tester) {
      return tester.pumpWidget(
        StoreProvider<AppState>(
          store: store,
          child: AnalyticsProvider(
            amplitudeAnalytics: amplitudeAnalytics,
            child: MaterialApp(
              onGenerateRoute: RouteGenerator.generateRoute,
              home: LoginPageContent(
                loginViewModel: loginViewModel,
              ),
              navigatorObservers: [observer],
            ),
          ),
        ),
      );
    }

    testWidgets('verify LoginPage exists and it containes LoginPageContent',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        StoreProvider<AppState>(
          store: store,
          child: MaterialApp(
            home: LoginPage(),
          ),
        ),
      );

      final loginPage = find.byType(LoginPage);
      expect(loginPage, findsOneWidget);

      final loginPageContent = find.byType(LoginPageContent);
      expect(loginPageContent, findsOneWidget);
    });

    testWidgets(
        'when LoginWithGoogleButton is clicked navigate to terms and conditions page on successful login',
        (WidgetTester tester) async {
      when(mockAuthProvider.signInWithGoogle())
          .thenAnswer((_) => Future<FirebaseUser>.value(firebaseUser));

      await _buildLoginPageContent(tester);
      verify(observer.didPush(any, any));

      final loginWithGoogleBtn = find.byKey(Key('LoginWithGoogleButton'));
      expect(loginWithGoogleBtn, findsOneWidget);

      await tester.tap(loginWithGoogleBtn);
      await tester.pumpAndSettle();

      verify(observer.didReplace(
          newRoute: anyNamed('newRoute'), oldRoute: anyNamed('oldRoute')));

      final termsAndConditionsPage = find.byType(TermsAndConditionsPage);
      expect(termsAndConditionsPage, findsOneWidget);
    });

    testWidgets(
        'when LoginWithGoogleButton is clicked display snackbar with error on failed login',
        (WidgetTester tester) async {
      final String errorMessage = 'Login error';
      final loginError = LoginException(errorMessage);

      when(mockAuthProvider.signInWithGoogle()).thenThrow(loginError);

      await _buildLoginPageContent(tester);
      verify(observer.didPush(any, any));

      final loginWithGoogleBtn = find.byKey(Key('LoginWithGoogleButton'));
      expect(loginWithGoogleBtn, findsOneWidget);

      await tester.tap(loginWithGoogleBtn);
      await tester.pumpAndSettle();

      final snackbarMessage = find.text(errorMessage);
      expect(snackbarMessage, findsOneWidget);
    });
  });
}
