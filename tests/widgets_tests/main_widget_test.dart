import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';

import 'package:member-app/features/login/pages/login_page.dart';
import 'package:member-app/main.dart';
import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/store.dart';

import '../mocks.dart';
import '../unit_tests/analytics/mocks.dart';
import 'mocks.dart';

void main() {
  group('member-appApp', () {
    MockAuthProvider mockAuthProvider;
    MockNavigatorObserver observer;
    Store<AppState> store;
    MockAmplitudeAnalytics amplitudeAnalytics;

    // final FirebaseUserMock firebaseUser = FirebaseUserMock();

    setUp(() {
      observer = MockNavigatorObserver();
      mockAuthProvider = MockAuthProvider();
      store = createStore(mockAuthProvider);
      amplitudeAnalytics = MockAmplitudeAnalytics();
    });

    Future<void> _buildmember-appApp(WidgetTester tester) {
      return tester.pumpWidget(
        member-appApp(
          store: store,
          amplitudeAnalytics: amplitudeAnalytics,
          navigatorObserver: observer,
        ),
      );
    }

    testWidgets('verify member-appApp exists and contains StoreProvider',
        (WidgetTester tester) async {
      await _buildmember-appApp(tester);

      final member-appApp = find.byType(member-appApp);
      expect(member-appApp, findsOneWidget);

      final storeProvider = find.byKey(Key('global-store-provider'));
      expect(storeProvider, findsOneWidget);
    });

    // testWidgets('should navigate to HomePage() if user logged in',
    //     (WidgetTester tester) async {
    //   when(mockAuthProvider.currentUser())
    //       .thenAnswer((_) => Future<FirebaseUser>.value(firebaseUser));

    //   await _buildmember-appApp(tester);

    //   final member-appApp = find.byType(member-appApp);
    //   expect(member-appApp, findsOneWidget);

    //   await tester.pumpAndSettle();
    //   verify(observer.didReplace(
    //     newRoute: anyNamed('newRoute'),
    //     oldRoute: anyNamed('oldRoute'),
    //   ));

    //   final homePage = find.byType(HomePage);
    //   expect(homePage, findsOneWidget);
    // });

    testWidgets('should navigate to LoginPage() if user not logged in',
        (WidgetTester tester) async {
      when(mockAuthProvider.currentUser())
          .thenAnswer((_) => Future<FirebaseUser>.value(null));

      await _buildmember-appApp(tester);

      final member-appApp = find.byType(member-appApp);
      expect(member-appApp, findsOneWidget);

      // await tester.pumpAndSettle();
      final loginPage = find.byType(LoginPage);
      expect(loginPage, findsOneWidget);
    });
  });
}
