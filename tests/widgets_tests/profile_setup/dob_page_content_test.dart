import 'package:member-app/features/profile_setup/redux/models/dob_viewmodel.dart';
import 'package:member-app/shared/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'package:redux/redux.dart';

import 'package:member-app/redux/models/app_state.dart';
import 'package:member-app/redux/store.dart';
import 'package:member-app/features/profile_setup/gender.dart';
import 'package:member-app/features/profile_setup/widgets/dob_page_content.dart';
import 'package:member-app/router/router_generator.dart';
import '../../mocks.dart';
import '../mocks.dart';

void main() {
  group('DobPageContent', () {
    MockNavigatorObserver observer;
    Store<AppState> store;
    MockAuthProvider mockAuthProvider;
    MockProfileProvider mockProfileProvider;
    DobViewmodel dobviewmodel;

    setUp(() {
      observer = MockNavigatorObserver();
      mockAuthProvider = MockAuthProvider();
      mockProfileProvider = MockProfileProvider();

      store = createStore(mockAuthProvider, mockProfileProvider);
      dobviewmodel = DobViewmodel.fromStore(store);
    });

    Widget getTestableWidget({Widget child}) {
      return MaterialApp(
        onGenerateRoute: RouteGenerator.generateRoute,
        home: child,
        navigatorObservers: [observer],
      );
    }

    testWidgets('DatePicker date can be selected', (WidgetTester tester) async {
      var dobPageContent = getTestableWidget(
          child: DobPageContent(
        dobViewModel: dobviewmodel,
      ));
      await tester.pumpWidget(dobPageContent);
      await tester.pumpAndSettle();

      var datePicker = find.byKey(DobPageContent.datePickerKey);
      expect(datePicker, findsOneWidget);

      await tester.tap(datePicker);
      await tester.pumpAndSettle();

      var dialog = find.byType(Dialog);
      expect(dialog, findsOneWidget);

      var dayInMonth = find.text('15');
      expect(dayInMonth, findsOneWidget);

      await tester.tap(dayInMonth);
      await tester.tap(find.text('OK'));
      await tester.pumpAndSettle();

      var dateNow = DateTime.now();
      var month = dateNow.month;
      var year = dateNow.year;
      var formatted = SILUtils.convertDateToString(
          date: DateTime(year, month, 15), format: 'dd-MM-yyyy');

      var selectedDate = find.text(formatted);
      expect(selectedDate, findsOneWidget);
    });

    testWidgets('genderPicker items can be selected',
        (WidgetTester tester) async {
      var dobPageContent = getTestableWidget(
          child: DobPageContent(
        dobViewModel: dobviewmodel,
      ));
      await tester.pumpWidget(dobPageContent);
      await tester.pumpAndSettle();

      var genderPicker = find.byKey(DobPageContent.genderPickerKey);
      expect(genderPicker, findsOneWidget);

      await tester.tap(genderPicker);
      await tester.pumpAndSettle();

      var genderItem = find.byKey(ValueKey(Gender.Male));
      expect(genderItem, findsOneWidget);

      await tester.tap(genderItem);
      await tester.pumpAndSettle();

      var maleMenuOption = find.byWidgetPredicate(
        (widget) {
          return widget is DropdownButton && widget.value == Gender.Male;
        },
        description: 'Male dropdown menu option',
      );

      expect(maleMenuOption, findsOneWidget);

      await tester.tap(genderPicker);
      await tester.pumpAndSettle();

      genderItem = find.byKey(ValueKey(Gender.Female));
      expect(genderItem, findsOneWidget);

      await tester.tap(genderItem);
      await tester.pumpAndSettle();

      var femaleMenuOption = find.byWidgetPredicate(
        (widget) {
          return widget is DropdownButton && widget.value == Gender.Female;
        },
        description: 'Female dropdown menu option',
      );

      expect(femaleMenuOption, findsOneWidget);
    });

    testWidgets('when next button is clicked navigates to home page',
        (WidgetTester tester) async {
      when(mockProfileProvider.saveDobPageDetails(
        gender: anyNamed('gender'),
        dob: anyNamed('dob'),
      )).thenReturn(null);

      var dobPageContent = getTestableWidget(
          child: DobPageContent(
        dobViewModel: dobviewmodel,
      ));
      await tester.pumpWidget(dobPageContent);
      await tester.pumpAndSettle();

      var nextButton = find.byKey(DobPageContent.nextButtonKey);
      expect(nextButton, findsOneWidget);
      expect(tester.widget<RaisedButton>(nextButton).enabled, isFalse);

      await tester.tap(find.byKey(DobPageContent.datePickerKey));
      await tester.pumpAndSettle();
      await tester.tap(find.text('15'));
      await tester.tap(find.text('OK'));
      await tester.pumpAndSettle();

      await tester.tap(find.byKey(DobPageContent.genderPickerKey));
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(ValueKey(Gender.Male)));
      await tester.pumpAndSettle();

      expect(tester.widget<RaisedButton>(nextButton).enabled, isTrue);

      // await tester.tap(nextButton);
      // await tester.pumpAndSettle();
      // verify(observer.didPush(any, any));

      // var homePage = ;
      // expect(homePage, findsOneWidget);
    });
  });
}
