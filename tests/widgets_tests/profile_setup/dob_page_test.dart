import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:redux/redux.dart';

import 'package:member-app/redux/store.dart';
import 'package:member-app/features/profile_setup/pages/dob_page.dart';
import 'package:member-app/features/profile_setup/widgets/dob_page_content.dart';
import 'package:member-app/redux/models/app_state.dart';

import '../../mocks.dart';

void main() {
  group('DobPage', () {
    Store<AppState> store;
    MockAuthProvider mockAuthProvider;

    setUp(() {
      mockAuthProvider = MockAuthProvider();
      store = createStore(mockAuthProvider);
    });

    testWidgets('verify DobPage exists', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: StoreProvider(
          store: store,
          child: DobPage(),
        ),
      ));

      final dobPage = find.byType(DobPage);
      final dobPageContent = find.byType(DobPageContent);

      expect(dobPage, findsOneWidget);
      expect(dobPageContent, findsOneWidget);
    });
  });
}
