import 'package:bewell/features/profile_setup/pages/pin_confirmation_setup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  //code goes here
  Widget makeTestableWidget({Widget child}) {
    return MaterialApp(
      home: child,
    );
  }

  testWidgets('test if widgets exist in pin_setup file',
      (WidgetTester tester) async {
    await tester.pumpWidget(makeTestableWidget(
        child: PinConfirmationSetup(
      arguments: 'StringArguments',
    )));

    var pinCodeTextField = find.byKey(Key('confirmation_pin_code_text_field'));
    expect(pinCodeTextField, findsOneWidget);
  });
}
