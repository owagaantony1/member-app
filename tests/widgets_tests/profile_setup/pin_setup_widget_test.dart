import 'package:member-app/features/profile_setup/pages/pin_setup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  Widget makeTestableWidget({Widget child}) {
    return MaterialApp(
      home: child,
    );
  }

  testWidgets('test if widgets exist in pin_setup file',
      (WidgetTester tester) async {
    await tester.pumpWidget(makeTestableWidget(child: PinSetup()));
    var pinCodeTextField = find.byKey(Key('pin_code_text_field'));
    expect(pinCodeTextField, findsOneWidget);

  });
}
