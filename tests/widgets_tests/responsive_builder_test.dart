import 'package:member-app/shared/widgets/responsive_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('OrientaionLayout', () {
    var portraitKey = Key('portrait');
    var landscapeKey = Key('landscape');

    Widget portrait() => Container(
          key: portraitKey,
          child: Text('Hello in portrait'),
        );

    Widget landscape() => Container(
          key: landscapeKey,
          child: Text('Hello in landscape'),
        );

    Widget testableWidget({Widget child, Size size}) {
      return MaterialApp(
        home: Scaffold(
          body: MediaQuery(
            data: MediaQueryData(size: size, textScaleFactor: 1),
            child: child,
          ),
        ),
      );
    }

    testWidgets(
        'should build and render orientation layout widget in landscape',
        (WidgetTester tester) async {
      var landscapeWidget = testableWidget(
        child: OrientationLayout(
          portrait: portrait(),
          landscape: landscape(),
        ),
        size: Size(1024, 768),
      );

      await tester.pumpWidget(landscapeWidget);
      final finder = find.byKey(landscapeKey);
      expect(finder, findsOneWidget);
    });

    testWidgets('should build and render orientation layout widget in portrait',
        (WidgetTester tester) async {
      var portraitWidget = testableWidget(
        child: OrientationLayout(
          portrait: portrait(),
          landscape: landscape(),
        ),
        size: Size(768, 1024),
      );

      await tester.pumpWidget(portraitWidget);
      final finder = find.byKey(portraitKey);
      expect(finder, findsOneWidget);
    });
  });

  // group('ResponsiveBuilder', () {
  //   Widget Function(BuildContext context, SizingInformation sizingInformation)
  //       builder;

  //   Widget testableWidget(
  //       {Widget child, Size size, SizingInformation sizingInformation}) {
  //     return MaterialApp(
  //       home: Scaffold(
  //         body: MediaQuery(
  //           data: MediaQueryData(size: size, textScaleFactor: 1),
  //           child: ResponsiveBuilder(
  //             builder:
  //                 (BuildContext context, SizingInformation sizingInformation) {
  //               return child;
  //             },
  //           ),
  //         ),
  //       ),
  //     );
  //   }

  //   testWidgets('should build and render responsive builder widget',
  //       (WidgetTester responsiveBuilder) async {
  //     await responsiveBuilder.pumpWidget(
  //       testableWidget(
  //         child: ResponsiveBuilder(
  //           builder: builder,
  //         ),
  //         size: Size(1024, 768),
  //         sizingInformation: SizingInformation(
  //           deviceScreenType: DeviceScreenType.Mobile,
  //           screenSize: Size(1024, 768),
  //           localWidgetSize: Size(100, 100),
  //         ),
  //       ),
  //     );
  //   });
  // });
}
