import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:member-app/shared/auth/auth_provider.dart';
import 'package:member-app/shared/widgets/sill_drawer.dart';
import 'package:member-app/redux/store.dart';

void main() {
  group('Test sil_drawer', () {
    GoogleSignIn _googleSignIn = GoogleSignIn();
    FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

    AuthProvider authProvider = AuthProvider(
      googleSignIn: _googleSignIn,
      firebaseAuth: _firebaseAuth,
    );
    final store = createStore(authProvider);
    testWidgets('SILDrawer', (WidgetTester tester) async {
      await tester.pumpWidget(
        StoreProvider(
          store: store,
          child: MaterialApp(home: Builder(
            builder: (BuildContext context) {
              return Scaffold(
                body: Builder(
                  builder: (BuildContext context) {
                    return Scaffold(
                      body: RaisedButton(
                        onPressed: () {
                          Scaffold.of(context).openDrawer();
                        },
                      ),
                    );
                  },
                ),
                drawer: Container(
                  width: MediaQuery.of(context).size.width,
                  child: SILDrawer(),
                ),
              );
            },
          )),
        ),
      );

      await tester.tap(find.byType(RaisedButton));
      await tester.pumpAndSettle();

      expect(find.byType(SILDrawer), findsOneWidget);
      await tester.tap(find.byKey(Key('view_profile')));
      await tester.pumpAndSettle();

      expect(find.text('View Profile'), findsOneWidget);

      await tester.tap(find.byKey(Key('logout_button')));
      await tester.pumpAndSettle();
      expect(find.text('Logout'), findsOneWidget);
    });
  });
}
