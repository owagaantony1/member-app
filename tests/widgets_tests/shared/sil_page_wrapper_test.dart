import 'package:member-app/shared/widgets/SIL_defer_back_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  group('Test that the back button does not work', () {
    testWidgets('SILPageWrapper', (WidgetTester tester) async {
      final mockObserver = MockNavigatorObserver();
      final pageTwo = SILDeferBackNavigation(
        child: Builder(
          builder: (BuildContext context) {
            return Scaffold(
              body: BackButton(
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        canPop: true,
      );
      await tester.pumpWidget(
        MaterialApp(
          navigatorObservers: [mockObserver],
          home: Builder(
            builder: (BuildContext context) {
              return Scaffold(
                body: RaisedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => pageTwo));
                  },
                ),
              );
            },
          ),
        ),
      );

      expect(find.byType(RaisedButton), findsOneWidget);
      await tester.tap(find.byType(RaisedButton));
      await tester.pumpAndSettle();

      // Use didPopRoute() to simulate the system back button. Check that
      // didPopRoute() indicates that the notification was handled.
      final dynamic widgetsAppState = tester.state(find.byType(WidgetsApp));
      expect(await widgetsAppState.didPopRoute(), isTrue);
    });
  });
}
