import 'package:member-app/features/profile_setup/pages/terms_and_conditions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  Widget makeTestableWidget ({Widget child}) {
    return MaterialApp(
      home: child,
    );
  }
  testWidgets('should test if widgets exist', (WidgetTester tester) async{

    await tester.pumpWidget(makeTestableWidget(child: TermsAndConditionsPage()));

    var checkBox  = find.byKey(Key('check_box'));
    expect(checkBox, findsOneWidget);
    var continueButton = find.byKey(Key('continue_button'));
    expect(continueButton, findsOneWidget);
    var cancelButton = find.byKey(Key('cancel_button'));
    expect(cancelButton, findsOneWidget);

    await tester.tap(checkBox);
    await tester.tap(continueButton);
    await tester.tap(cancelButton);

    await tester.pumpAndSettle();

  });
}